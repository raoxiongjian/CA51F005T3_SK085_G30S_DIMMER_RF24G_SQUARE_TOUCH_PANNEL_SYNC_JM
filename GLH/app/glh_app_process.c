/*
* Copyright (c) 2021, 深圳市光丽海科技有限公司
* All rights reserved.
*
* 文件名称：glh_app_process.c
* 文件标识：
* 摘 要：
*   主要对私有化的任务做处理
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期：2021年7月6日
*/
#include "glh_pwm.h"
#include "glh_app_process.h"
#include "glh_sys_tick.h"
#include "glh_led_mode.h"
#include "glh_flash.h"
#include "glh_lt8920l.h"
#include "glh_watchdog.h"
#include "glh_over_current_protect.h"
#include "delay.h"

#define SC_CONFIG_ADDR1           0
#define SC_CONFIG_ADDR2           256
#define SC_CONFIG_ADDR3           512
#define FLASH_WRITE_FLAG         0x55             //写入过数据的标志
#define SAVE_INTERVER            500
#define SC_CONFIG_BUFF_NUM       4
#define R_INDEX                  0
#define G_INDEX                  1
#define B_INDEX                  2
#define W_INDEX                  3
#define ADDR_NUM_MAX             4
#define EMPTY_ADDR               0
#define EMPTY_GROUP              0
#define APS_BN_MAX               100
#define APS_BN_MIN               1
#define APS_CW_MAX               100
#define APS_CW_MIN               0
#define APS_CW_CHANGE            20
#define APS_BN_CHANGE            10
#define APS_SPEED_MAX            10
#define APS_SPEED_MIN            1
#define APS_DEFAULT_SPEED        5
#define APS_SPEED_CHANGE         1
#define APS_H_GRID               20
#define R_MAX_POSITION_WHEEL_NUM        0
#define B_MAX_POSITION_WHEEL_NUM        64
#define G_MAX_POSITION_WHEEL_NUM        128
#define WHEEL_NUM_MAX                   255
#define NO_NEED_PAIR                    0x01
#define NEED_PAIR                       0x02
#define FACTORY_TEST_RSSI_OK_VALUE      180
#define RSSI_NUM                        8        //测试模式下需要采集几次的RSSI值来求平均值，从而判断是否合格

#define GROUP_ALL 0
#define REMOTER_BUFF_LEN   13
#define IDENTIFICATION_CODE_INDEX      0
#define DATA_POINT_INDEX               1
#define KEY_CODE_INDEX                 2
#define PACK_NUM_INDEX                 3
#define ADDR1_INDEX                    4
#define ADDR2_INDEX                    5
#define ADDR3_INDEX                    6
#define ZID_INDEX                      7
#define GROUP_NUM_INDEX                8
#define RANDOM_INDEX                   9
#define VALUE1_INDEX                   10
#define VALUE2_INDEX                   11

#define DPID_SWITCH                    1
#define DPID_ON                        2
#define DPID_OFF                       3
#define DPID_BRIGHTNESS_SET            4
#define DPID_BRIGHTNESS_INC            5
#define DPID_BRIGHTNESS_DEC            6
#define DPID_CW_SET                    7
#define DPID_CW_INC                    8
#define DPID_CW_DEC                    9
#define DPID_COLOUR_SET                10
#define DPID_COLOUR_INC                11
#define DPID_COLOUR_DEC                12
#define DPID_SATURATION_SET            13
#define DPID_SATURATION_INC            14
#define DPID_SATURATION_DEC            15
#define DPID_SPEED_SET                 16
#define DPID_SPEED_INC                 17
#define DPID_SPEED_DEC                 18
#define DPID_MODE_SET                  19
#define DPID_MODE_INC                  20
#define DPID_MODE_DEC                  21
#define DPID_SINGLE_WHITE              22
#define DPID_SCENCE_SAVE               23
#define DPID_SCENCE_EXCUTE             24
#define DPID_NIGHT_LIGHT               25
#define DPID_MIX_WHETE                 26
#define DPID_TIMING                    27

#define KEY_BREAK			0x1000     	//短按抬起
#define KEY_LONG			0x2000     	//长按
#define KEY_LONG_BREAK		0x3000		//长按抬起
#define KEY_LONG_START		0x4000		//长按开始

#ifndef S_MAX 
#define S_MAX                    1000.0
#endif

#ifndef V_MAX 
#define V_MAX                    1000.0
#endif

#ifndef H_MAX 
#define H_MAX                    360
#endif

#define SEHUAN_VALUE_MAX         255

typedef enum _DEVICE_TYPE_E
{
    E_DEVICE_TYPE_UNKNOWN = 0,
	E_DEVICE_TYPE_SWITCH,
    E_DEVICE_TYPE_DIMMER,
    E_DEVICE_TYPE_CW,
    E_DEVICE_TYPE_RGB,
    E_DEVICE_TYPE_RGBW,
    E_DEVICE_TYPE_RGBCCT,
} DEVICE_TYPE_E;

typedef enum _SWITCH_STATUS_E
{
	E_SWITCH_STATUS_ON = 0,
    E_SWITCH_STATUS_OFF ,
    E_SWITCH_STATUS_NIGHT_LIGHT,
} SWITCH_STATUS_E;

typedef enum _WORK_MODE_TYPE_E
{
    E_WORK_MODE_TYPE_COLOUR,
    E_WORK_MODE_TYPE_WHITE, 
    E_WORK_MODE_TYPE_SCENCE,
    E_WORK_MODE_NIGHT_LIGHT,
    E_WORK_MODE_MIX_WHITE,
    E_WORK_MODE_ALL_WHITE,
}WORK_MODE_TYPE_E;

typedef enum _SCENCE_TYPE_E
{
    E_SCENCE_TYPE_AUTO = 0,
    E_SCENCE_TYPE_JUMP_RGB,
    E_SCENCE_TYPE_BREATH_RGB,
    E_SCENCE_TYPE_JUMP_RGBYVCW,
    E_SCENCE_TYPE_FLASH_RGBYVCW,
    E_SCENCE_TYPE_BREATH_RGBYVCW,
    E_SCENCE_TYPE_FADE_RGB,   
    E_SCENCE_TYPE_BREATH_W,
    E_SCENCE_TYPE_1BREATH3JUMP_R,
    E_SCENCE_TYPE_1BREATH3JUMP_G,
    E_SCENCE_TYPE_1BREATH3JUMP_B,
    E_SCENCE_TYPE_1BREATH3JUMP_W,
    E_SCENCE_TYPE_MAX,
}SCENCE_TYPE_E;

typedef struct _SC_CONFIG_S
{
    //所有设备类型共用的配置参数
	uint8 u8SaveFlag;                      //数据是否有保存过的标记
    DEVICE_TYPE_E eDeviceType;             //设备类型
	uint32 au32Addr[ADDR_NUM_MAX];         //地址码，或者叫配对码的存储数组
	uint8 au8Group[ADDR_NUM_MAX];          //分组号
    uint32 u32CurrAddr;                    //记录最后一次是被哪一个遥控器控制的，同步的时候用
    uint8 u8CurrGroup;
	BOOL bIsNeedPair;                      //是否需要配对
	uint8 u8PairNum;                       //已配对遥控器的数量
	SWITCH_STATUS_E eSwitch;               //开关状态
	uint8 u8ColourfulDeviceBn;             //亮度值
	uint8 u8ColourfulDeviceSpeed;          //速度值
	uint8 u8ColourfulDeviceaRgb[4];        //RGB颜色数据
    uint8 u8ColourTemprature;              //色温值
	SCENCE_TYPE_E eScence;                 //情景模式
    WORK_MODE_TYPE_E eWorkModeType;        //工作模式
    uint8 u8Reserved[5];
} SC_CONFIG_S;

static SC_CONFIG_S s_sScConfig[SC_CONFIG_BUFF_NUM] = {0};
static SC_CONFIG_S s_PreScConfig[SC_CONFIG_BUFF_NUM] = {0};
static SCENCE_TYPE_E s_eModeAuto = E_SCENCE_TYPE_JUMP_RGB;  //用来记录自动模式执行到了哪一个模式
static BOOL s_bIsRgbControledByPair = FALSE;
static uint8 s_u8PairFlashCnt = 0;
static uint32 s_u32PairFlashTime = 0;
static uint8 s_u8PairFlashFlag = 0;
static uint8 s_au8Rf24gRecBuff[64] = {0};
static uint8 u8Rf24gRecLen = 0;
static uint32 s_u32LastChannelChangeTime = 0;
static uint8 s_bPairFlag = FALSE;
static BOOL s_bTimingPowerOffFlag = FALSE;
static uint32 s_u32TimingStartTime = 0;     //定时开始的时间
static uint32 s_u32TimingTime = 0;          //定时的时长
#define RF24G_CHANNEL_NUM  3
#define RF24G_CH      au8ChannelTable[s_u8Channel]      
CODE uint8 au8ChannelTable[RF24G_CHANNEL_NUM] = {0x08, 0x4a, 0x74};
static uint8 s_u8Channel = 0;
static uint32 s_u32SteplessBrightnessAdjustmentStartTime = 0;
static BOOL s_bOverCurrentFlag = FALSE;
static uint32 s_u32OverCurrentStartTime = 0;
static BOOL s_bApsIsOverCurrent = FALSE;

#define SYNC_BUFF_LEN         26
#define HEAD_CODE          0x68
#define TX_HEAD_INDEX       0
#define TX_ADDR0_INDEX      1
#define TX_ADDR1_INDEX      2
#define TX_ADDR2_INDEX      3
#define TX_ADDR3_INDEX      4
#define TX_GROUP_INDEX      5
#define TX_SWITCH_INDEX     6
#define TX_MODE_INDEX       7
#define TX_SCENCE_INDEX     8
#define TX_R_INDEX          9
#define TX_G_INDEX          10
#define TX_B_INDEX          11
#define TX_W_INDEX          12
#define TX_SPEED_INDEX      13
#define TX_BN_INDEX         14
#define TX_CURRENT_R_INDEX  15
#define TX_CURRENT_G_INDEX  16
#define TX_CURRENT_B_INDEX  17
#define TX_CURRENT_W_INDEX  18
#define TX_PACK_NUM_INDEX   19
#define TX_AUTO_MODE_INDEX  20
#define TX_U8INDEX_INDEX    21
#define TX_STEP_INDEX       22
#define TX_MODE_FLAG_INDEX  23
#define TX_DEVICE_TYPE_INDEX  24
#define TX_CHECK_INDEX      25

static uint32 s_u32LastSyncTime = 0;      //上一次接收到同步数据的时间
static uint8 s_au8TxBuff[SYNC_BUFF_LEN] = {0};
static BOOL ModeKeyFlag = FALSE;
static void iRgbFlash(uint8 u8Cnt, uint16 u16Time);
static uint8 s_u8PackNum = 0;
void ModeComepleteSyncSend(void)
{
    uint8 i = 0;
    
    if(s_sScConfig[0].eSwitch != E_SWITCH_STATUS_ON)
    {
        return;
    }
    
    if(s_sScConfig[0].u32CurrAddr == EMPTY_ADDR)
    {
        return;
    }

    if(GulSystickCount - s_u32LastSyncTime < 3000)
    {
        return;
    }
    
    s_u8PackNum++;
    
    s_au8TxBuff[TX_HEAD_INDEX] = HEAD_CODE;
    s_au8TxBuff[TX_ADDR0_INDEX] = s_sScConfig[0].u32CurrAddr >> 24;
    s_au8TxBuff[TX_ADDR1_INDEX] = s_sScConfig[0].u32CurrAddr >> 16;
    s_au8TxBuff[TX_ADDR2_INDEX] = s_sScConfig[0].u32CurrAddr >> 8;
    s_au8TxBuff[TX_ADDR3_INDEX] = s_sScConfig[0].u32CurrAddr;
    s_au8TxBuff[TX_GROUP_INDEX] = s_sScConfig[0].u8CurrGroup;
    s_au8TxBuff[TX_SWITCH_INDEX] = s_sScConfig[0].eSwitch;
    s_au8TxBuff[TX_MODE_INDEX] = s_sScConfig[0].eWorkModeType;
    s_au8TxBuff[TX_SCENCE_INDEX] = s_sScConfig[0].eScence;
    s_au8TxBuff[TX_R_INDEX] = s_sScConfig[0].u8ColourfulDeviceaRgb[0];
    s_au8TxBuff[TX_G_INDEX] = s_sScConfig[0].u8ColourfulDeviceaRgb[1];
    s_au8TxBuff[TX_B_INDEX] = s_sScConfig[0].u8ColourfulDeviceaRgb[2];
    s_au8TxBuff[TX_W_INDEX] = s_sScConfig[0].u8ColourfulDeviceaRgb[3];
    s_au8TxBuff[TX_SPEED_INDEX] = s_sScConfig[0].u8ColourfulDeviceSpeed;
    s_au8TxBuff[TX_BN_INDEX] = s_sScConfig[0].u8ColourfulDeviceBn;
    GLHLM_GetCurrRgbw(s_au8TxBuff+TX_CURRENT_R_INDEX, s_au8TxBuff+TX_CURRENT_G_INDEX, s_au8TxBuff+TX_CURRENT_B_INDEX, s_au8TxBuff+TX_CURRENT_W_INDEX);
    s_au8TxBuff[TX_PACK_NUM_INDEX] = s_u8PackNum;
    s_au8TxBuff[TX_AUTO_MODE_INDEX] = s_eModeAuto;
    s_au8TxBuff[TX_DEVICE_TYPE_INDEX] = s_sScConfig[0].eDeviceType;
    
    s_au8TxBuff[TX_U8INDEX_INDEX] = GLHLM_GetCurrColorIndex();
    s_au8TxBuff[TX_STEP_INDEX] = GLHLM_GetStep();
    s_au8TxBuff[TX_MODE_FLAG_INDEX] = ModeKeyFlag;
    ModeKeyFlag = FALSE;
    
    s_au8TxBuff[TX_CHECK_INDEX] = 0;
    for(i=0; i<SYNC_BUFF_LEN-1; i++)
    {
        s_au8TxBuff[TX_CHECK_INDEX] += s_au8TxBuff[i];
    } 
    
    for(i=0; i<10; i++)
    {
        s_u8Channel++;
        if(s_u8Channel >= RF24G_CHANNEL_NUM)
        {
            s_u8Channel = 0;
        }
        
        RF24G_Tx(s_au8TxBuff, SYNC_BUFF_LEN, RF24G_CH);
        while(RF24G_IsTxOrRxComplete() == E_RF24G_REC_STATUS_NO)
        {
        
        }
        Delay_ms((GulSystickCount % 3) + 1);
    }
    
    RF24G_EnterRxMode(RF24G_CH);
    s_u32LastChannelChangeTime = GulSystickCount;
}

static void iRf24gReceiveMainThread(void);
static void iRf24gRemoterDataHandle(uint8 *pu8RxData, uint8 *pu8RxLen);
static void iRf24gSyncDataHandle(uint8 *pu8RxData, uint8 *pu8RxLen);
void RgbToHsv(uint16 *pu16H, uint16 *pu16S, uint16 *pu16V, uint8 u8R, uint8 u8G, uint8 u8B);
void HsvToRgb(int16 h, int16 s, int16 v, uint8 *pu8R, uint8 *pu8G, uint8 *pu8B);
static void iBnInc(void);
static void iBnDec(void);
static void iBnSet(uint8 u8Bn);
static void iSpeedInc(void);
static void iSpeedDec(void);
static void iModeInc(void);
static void iModeDec(void);
static void iColorInc(void);
static void iColorDec(void);
static void iCwInc(void);
static void iCwDec(void);
static void iTimingPowerOffMainThread(void);
static void iRf24gChannelChangeMainThread(void);
static void iSteplessBrightnessAdjustmentMainThread(void);
void ModeComepleteSyncSend(void);

//复位模式参数
static void iResetScConfig(void)
{
    uint8 i = 0;
    
    s_sScConfig[0].eDeviceType = DEFAULT_DEVEICE_TYPE;
	s_sScConfig[0].eSwitch = E_SWITCH_STATUS_ON;
	s_sScConfig[0].u8ColourfulDeviceBn = APS_BN_MAX;
	s_sScConfig[0].u8ColourfulDeviceSpeed = APS_DEFAULT_SPEED;
	s_sScConfig[0].u8ColourfulDeviceaRgb[R_INDEX] = COLOUR_MAX;
	s_sScConfig[0].u8ColourfulDeviceaRgb[G_INDEX] = 0;
	s_sScConfig[0].u8ColourfulDeviceaRgb[B_INDEX] = 0;
    s_sScConfig[0].eWorkModeType = E_WORK_MODE_TYPE_COLOUR;
	s_sScConfig[0].eScence = E_SCENCE_TYPE_JUMP_RGB;
	s_sScConfig[0].bIsNeedPair = TRUE;    //默认通码
	s_sScConfig[0].u8PairNum = 0;
	
    for(i=0; i<ADDR_NUM_MAX; i++)
    {
        s_sScConfig[0].au32Addr[i] = EMPTY_ADDR;
        s_sScConfig[0].au8Group[i] = EMPTY_GROUP;
    }
    
    for(i=0; i<SC_CONFIG_BUFF_NUM - 1; i++)
    {
        memcpy(&s_sScConfig[i+1], &s_sScConfig[0], sizeof(SC_CONFIG_S));
    }
}

static void iSlectScence(SCENCE_TYPE_E eScence)
{
	switch(eScence)
	{
		case E_SCENCE_TYPE_AUTO:
		{
			GLHLM_SetColorBuff(0, COLOUR_MAX, 0, 0, 0, 0);
			GLHLM_SetColorBuff(1, 0, COLOUR_MAX, 0, 0, 0);
			GLHLM_SetColorBuff(2, 0, 0, COLOUR_MAX, 0, 0);
			GLHLM_JumpMode(3);
			s_eModeAuto = E_SCENCE_TYPE_JUMP_RGB;
		}
		break;
        
		case E_SCENCE_TYPE_JUMP_RGB:
		{
			GLHLM_SetColorBuff(0, COLOUR_MAX, 0, 0, 0, 0);
			GLHLM_SetColorBuff(1, 0, COLOUR_MAX, 0, 0, 0);
			GLHLM_SetColorBuff(2, 0, 0, COLOUR_MAX, 0, 0);
			GLHLM_JumpMode(3);
		}
		break;
        
		case E_SCENCE_TYPE_BREATH_RGB:
		{
			GLHLM_SetColorBuff(0, COLOUR_MAX, 0, 0, 0, 0);
            GLHLM_SetColorBuff(1, 0, COLOUR_MAX, 0, 0, 0);
            GLHLM_SetColorBuff(2, 0, 0, COLOUR_MAX, 0, 0);
			GLHLM_BreathMode(3);
		}
		break;
        
		case E_SCENCE_TYPE_JUMP_RGBYVCW:
		{
			GLHLM_SetColorBuff(0, COLOUR_MAX, 0, 0, 0, 0);
            GLHLM_SetColorBuff(1, 0, COLOUR_MAX, 0, 0, 0);
            GLHLM_SetColorBuff(2, 0, 0, COLOUR_MAX, 0, 0);
            GLHLM_SetColorBuff(3, COLOUR_MAX, COLOUR_MAX, 0, 0, 0);
            GLHLM_SetColorBuff(4, COLOUR_MAX, 0, COLOUR_MAX, 0, 0);
            GLHLM_SetColorBuff(5, 0, COLOUR_MAX, COLOUR_MAX, 0, 0);
            GLHLM_SetColorBuff(6, 0, 0, 0, COLOUR_MAX, 0);
			GLHLM_JumpMode(7);
		}
		break;
        
		case E_SCENCE_TYPE_FLASH_RGBYVCW:
		{
			GLHLM_SetColorBuff(0, COLOUR_MAX, 0, 0, 0, 0);
            GLHLM_SetColorBuff(1, 0, COLOUR_MAX, 0, 0, 0);
            GLHLM_SetColorBuff(2, 0, 0, COLOUR_MAX, 0, 0);
            GLHLM_SetColorBuff(3, COLOUR_MAX, COLOUR_MAX, 0, 0, 0);
            GLHLM_SetColorBuff(4, COLOUR_MAX, 0, COLOUR_MAX, 0, 0);
            GLHLM_SetColorBuff(5, 0, COLOUR_MAX, COLOUR_MAX, 0, 0);
            GLHLM_SetColorBuff(6, 0, 0, 0, COLOUR_MAX, 0);
			GLHLM_FlashMode(7);
		}
		break;
        
		case E_SCENCE_TYPE_FADE_RGB:   
		{
			GLHLM_SetColorBuff(0, COLOUR_MAX, 0, 0, 0, 0);
            GLHLM_SetColorBuff(1, 0, COLOUR_MAX, 0, 0, 0);
            GLHLM_SetColorBuff(2, 0, 0, COLOUR_MAX, 0, 0);
			GLHLM_FadeMode(3);
		}
		break;
        
		case E_SCENCE_TYPE_BREATH_RGBYVCW:   
		{
			GLHLM_SetColorBuff(0, COLOUR_MAX, 0, 0, 0, 0);
            GLHLM_SetColorBuff(1, 0, COLOUR_MAX, 0, 0, 0);
            GLHLM_SetColorBuff(2, 0, 0, COLOUR_MAX, 0, 0);
            GLHLM_SetColorBuff(3, COLOUR_MAX, COLOUR_MAX, 0, 0, 0);
            GLHLM_SetColorBuff(4, COLOUR_MAX, 0, COLOUR_MAX, 0, 0);
            GLHLM_SetColorBuff(5, 0, COLOUR_MAX, COLOUR_MAX, 0, 0);
            GLHLM_SetColorBuff(6, 0, 0, 0, COLOUR_MAX, 0);
			GLHLM_BreathMode(7);
		}
		break;
        
		case E_SCENCE_TYPE_BREATH_W:   
		{
			GLHLM_SetColorBuff(0, 0, 0, 0, COLOUR_MAX, 0);
			GLHLM_BreathMode(1);
		}
		break;
		
		case E_SCENCE_TYPE_1BREATH3JUMP_R:   
		{
			GLHLM_SetColorBuff(0, COLOUR_MAX, 0, 0, 0, 0);
			GLHLM_OneBthAndThreeJumpMode(1);
		}
		break;
        
		case E_SCENCE_TYPE_1BREATH3JUMP_G:   
		{
			GLHLM_SetColorBuff(0, 0, COLOUR_MAX, 0, 0, 0);
			GLHLM_OneBthAndThreeJumpMode(1);
		}
		break;
        
		case E_SCENCE_TYPE_1BREATH3JUMP_B:   
		{
			GLHLM_SetColorBuff(0, 0, 0, COLOUR_MAX, 0, 0);
			GLHLM_OneBthAndThreeJumpMode(1);
		}
		break;
        
		case E_SCENCE_TYPE_1BREATH3JUMP_W:   
		{
			GLHLM_SetColorBuff(0, 0, 0, 0, COLOUR_MAX, 0);
			GLHLM_OneBthAndThreeJumpMode(1);
		}
		break;
        
		default:
		break;
	}
}

static void iRefreshAsConfig(void)
{
    if(s_sScConfig[0].eSwitch == E_SWITCH_STATUS_OFF)
	{
        GLHLM_SetSwitchStatus(OFF);
		return;
	}
    
    GLHLM_SetSwitchStatus(ON);

    switch(s_sScConfig[0].eDeviceType)
    {
#ifdef SUPPORT_DEVIVE_TYPE_DIMMER
        case E_DEVICE_TYPE_DIMMER:
        {
            GLHLM_SetBn(s_sScConfig[0].u8ColourfulDeviceBn);
            GLHLM_StaticMode(COLOUR_MAX, COLOUR_MAX, COLOUR_MAX, COLOUR_MAX, 0);
        }
        break;
#endif

#ifdef SUPPORT_DEVIVE_TYPE_RGB        
        case E_DEVICE_TYPE_RGB:
        {
            if(s_sScConfig[0].eSwitch == E_SWITCH_STATUS_NIGHT_LIGHT)
            {
                GLHLM_SetBn(s_sScConfig[0].u8ColourfulDeviceBn);
                GLHLM_StaticMode(APS_BN_MIN * COLOUR_MAX / s_sScConfig[0].u8ColourfulDeviceBn, APS_BN_MIN * COLOUR_MAX / s_sScConfig[0].u8ColourfulDeviceBn,\
                APS_BN_MIN * COLOUR_MAX / s_sScConfig[0].u8ColourfulDeviceBn, 0, 0);
                return;
            }
    
            switch(s_sScConfig[0].eWorkModeType)
            {
                case E_WORK_MODE_TYPE_COLOUR:
                {
                    GLHLM_StaticMode(s_sScConfig[0].u8ColourfulDeviceaRgb[R_INDEX], s_sScConfig[0].u8ColourfulDeviceaRgb[G_INDEX], s_sScConfig[0].u8ColourfulDeviceaRgb[B_INDEX], 0, 0);           
                }
                break;
            
                case E_WORK_MODE_MIX_WHITE:
                {
                    GLHLM_StaticMode(COLOUR_MAX, COLOUR_MAX, COLOUR_MAX, 0, 0);
                }
                break;
            
                case E_WORK_MODE_TYPE_SCENCE:
                {
                    iSlectScence(s_sScConfig[0].eScence);
                }
                break;
            }
        }
        break;
#endif

#ifdef SUPPORT_DEVIVE_TYPE_RGBW        
        case E_DEVICE_TYPE_RGBW:
        {
            if(s_sScConfig[0].eSwitch == E_SWITCH_STATUS_NIGHT_LIGHT)
            {
                GLHLM_SetBn(s_sScConfig[0].u8ColourfulDeviceBn);
                GLHLM_StaticMode(0, 0, 0, APS_BN_MIN * COLOUR_MAX / s_sScConfig[0].u8ColourfulDeviceBn, 0);                
                return;
            }
    
            switch(s_sScConfig[0].eWorkModeType)
            {
                case E_WORK_MODE_TYPE_COLOUR:
                {
                    GLHLM_StaticMode(s_sScConfig[0].u8ColourfulDeviceaRgb[R_INDEX], s_sScConfig[0].u8ColourfulDeviceaRgb[G_INDEX], s_sScConfig[0].u8ColourfulDeviceaRgb[B_INDEX], 0, 0);           
                }
                break;
            
                case E_WORK_MODE_TYPE_WHITE:
                {
                    GLHLM_StaticMode(0, 0, 0, COLOUR_MAX, 0);   
                }
                break;
                
                case E_WORK_MODE_MIX_WHITE:
                {
                    GLHLM_StaticMode(COLOUR_MAX, COLOUR_MAX, COLOUR_MAX, 0, 0);   
                }
                break;
                
                case E_WORK_MODE_ALL_WHITE:
                {
                    GLHLM_StaticMode(COLOUR_MAX, COLOUR_MAX, COLOUR_MAX, COLOUR_MAX, 0);   
                }
                break;
            
                case E_WORK_MODE_TYPE_SCENCE:
                {
                    iSlectScence(s_sScConfig[0].eScence);
                }
                break;
            }
        }
        break;
#endif
       default:
           break;
    }
    
    
    GLHLM_SetBn(s_sScConfig[0].u8ColourfulDeviceBn);
	GLHLM_SetSpeed(s_sScConfig[0].u8ColourfulDeviceSpeed);
}

static void iInitScConfig(void)
{
	memset((uint8 *)s_sScConfig, 0, sizeof(s_sScConfig));
	GLHFLASH_ReadBytes(SC_CONFIG_ADDR1, (uint8 *)s_sScConfig, sizeof(s_sScConfig));
    memcpy((uint8 *)(s_PreScConfig), (uint8 *)(&s_sScConfig), sizeof(s_sScConfig));
    if((s_sScConfig[0].u8SaveFlag != FLASH_WRITE_FLAG))
    {
        s_sScConfig[0].u8SaveFlag = FLASH_WRITE_FLAG;

        iResetScConfig();
    }
	iRefreshAsConfig();
}

static void iSaveScConfigMainThread(void)
{
	static uint32 s_u32LastSaveTime = 0 - SAVE_INTERVER;    

    if(GulSystickCount - s_u32LastSaveTime >= SAVE_INTERVER)	//1s检查一次是否需要保存
	{
		s_u32LastSaveTime = GulSystickCount;
		
		if(0 == memcmp((uint8 *)(s_PreScConfig),(uint8 *)s_sScConfig,sizeof(s_sScConfig)))
		{
			return;
		}
		
		memcpy((uint8 *)(s_PreScConfig),(uint8 *)(s_sScConfig),sizeof(s_sScConfig));
	    GLHFLASH_WriteBytes(SC_CONFIG_ADDR1, (uint8 *)(s_PreScConfig),sizeof(s_sScConfig));
	}
}

void GLHAPS_Init(void)
{
	GLHST_Init();
    while(GulSystickCount < 200);    //等待上电稳定
    GLHFLASH_Init();
    GLHLM_Init();
    RF24G_Init();
    RF24G_EnterRxMode(au8ChannelTable[s_u8Channel]);
	GLHWD_Init();
	iInitScConfig();
    GLHOCP_Init();
}

void GLHAPS_MainThread(void)
{
    GLHWD_Clear();
    GLHOCP_MainThread();
    if(s_bApsIsOverCurrent == TRUE)
    {
        return;
    } 
	iSaveScConfigMainThread();
    iRf24gReceiveMainThread();
    iTimingPowerOffMainThread();
    iRf24gChannelChangeMainThread();
    GLHLM_RenderProc();
    RF24G_IsWorkingNomalCheckMainThread();
	
	if(s_sScConfig[0].eScence == E_SCENCE_TYPE_AUTO)
    {
		if(GLHLM_GetModeIsComplete() == TRUE)
		{
			s_eModeAuto++;
			if((uint8)s_eModeAuto >= (uint8)E_SCENCE_TYPE_MAX)
			{
				s_eModeAuto = E_SCENCE_TYPE_JUMP_RGB;
			}
			iSlectScence(s_eModeAuto);
		}
	}
}

static void iRgbflash(uint8 u8Num, uint32 u32Time)
{

    uint32 u32WatiStartTime = 0;
    uint8 i = 0;
    
    for(i=0; i<u8Num; i++)
    {
        GLHPWM_Set(PWM_CHANNEL_R, GLHPWM_MAX);
        GLHPWM_Set(PWM_CHANNEL_G, GLHPWM_MAX);
        GLHPWM_Set(PWM_CHANNEL_B, GLHPWM_MAX);
        GLHPWM_Set(PWM_CHANNEL_C, GLHPWM_MAX);
        GLHPWM_Set(PWM_CHANNEL_W, GLHPWM_MAX);
        u32WatiStartTime = GulSystickCount;
        while(GulSystickCount - u32WatiStartTime < u32Time)
        {
            GLHWD_Clear();
        }
        
        GLHPWM_Set(PWM_CHANNEL_R, 0);
        GLHPWM_Set(PWM_CHANNEL_G, 0);
        GLHPWM_Set(PWM_CHANNEL_B, 0);
        GLHPWM_Set(PWM_CHANNEL_C, 0);
        GLHPWM_Set(PWM_CHANNEL_W, 0);
        u32WatiStartTime = GulSystickCount;
        while(GulSystickCount - u32WatiStartTime < u32Time)
        {
            GLHWD_Clear();
        }
    }
    iRefreshAsConfig();
}

static void iBnInc(void)
{
    if(s_sScConfig[0].eSwitch != E_SWITCH_STATUS_ON)
    {
		return;
	}
			
	if(s_sScConfig[0].u8ColourfulDeviceBn + APS_BN_CHANGE < APS_BN_MAX)
	{
		s_sScConfig[0].u8ColourfulDeviceBn += APS_BN_CHANGE;
	}
	else
	{
		s_sScConfig[0].u8ColourfulDeviceBn = APS_BN_MAX;
	}
	GLHLM_SetBn(s_sScConfig[0].u8ColourfulDeviceBn);
}

static void iBnDec(void)
{
    if(s_sScConfig[0].eSwitch != E_SWITCH_STATUS_ON)
    {
		return;
    }
    	
	if(s_sScConfig[0].u8ColourfulDeviceBn - APS_BN_CHANGE > APS_BN_MIN)
	{
		s_sScConfig[0].u8ColourfulDeviceBn -= APS_BN_CHANGE;
	}
	else
	{
		s_sScConfig[0].u8ColourfulDeviceBn = APS_BN_MIN;
	}
	GLHLM_SetBn(s_sScConfig[0].u8ColourfulDeviceBn);
}

static void iBnSet(uint8 u8Bn)
{
    if(s_sScConfig[0].eSwitch != E_SWITCH_STATUS_ON)
    {
		return;
    }
    
    if(u8Bn > APS_BN_MAX)
    {
        s_sScConfig[0].u8ColourfulDeviceBn = APS_BN_MAX;
    }
    else if(u8Bn < APS_BN_MIN)
    {
        s_sScConfig[0].u8ColourfulDeviceBn = APS_BN_MIN;
    }
    else
    {
        s_sScConfig[0].u8ColourfulDeviceBn = u8Bn;
    }
    GLHLM_SetBn((float)s_sScConfig[0].u8ColourfulDeviceBn);
}

static void iSpeedInc(void)
{
//    static uint32 s_u32LastSpeedIncTime = 0;
    
    
    if(s_sScConfig[0].eSwitch != E_SWITCH_STATUS_ON)
    {
		return;
	}
			
	if((s_sScConfig[0].eWorkModeType != E_WORK_MODE_TYPE_SCENCE))       //不是情景模式，即静态模式下不去调速度
	{
		return; 
	}
    
//    if(GulSystickCount - s_u32LastSpeedIncTime < 200)
//    {
//        return;
//    }
//    s_u32LastSpeedIncTime = GulSystickCount;
			
	if(s_sScConfig[0].u8ColourfulDeviceSpeed + APS_SPEED_CHANGE < APS_SPEED_MAX)
	{
		s_sScConfig[0].u8ColourfulDeviceSpeed += APS_SPEED_CHANGE;
	}
	else
	{
		s_sScConfig[0].u8ColourfulDeviceSpeed = APS_SPEED_MAX;
	}
	GLHLM_SetSpeed(s_sScConfig[0].u8ColourfulDeviceSpeed);
}

static void iSpeedDec(void)
{
//    static uint32 s_u32LastSpeedDecTime = 0;
    
    if(s_sScConfig[0].eSwitch != E_SWITCH_STATUS_ON)
    {
		return;
	}
			
	if((s_sScConfig[0].eWorkModeType != E_WORK_MODE_TYPE_SCENCE))       //不是情景模式，即静态模式下不去调速度
	{
		return; 
	}
    
//    if(GulSystickCount - s_u32LastSpeedDecTime < 200)
//    {
//        return;
//    }
//    s_u32LastSpeedDecTime = GulSystickCount;
			
	if(s_sScConfig[0].u8ColourfulDeviceSpeed - APS_SPEED_CHANGE > APS_SPEED_MIN)
	{
		s_sScConfig[0].u8ColourfulDeviceSpeed -= APS_SPEED_CHANGE;
	}
	else
	{
		s_sScConfig[0].u8ColourfulDeviceSpeed = APS_SPEED_MIN;
	}
	GLHLM_SetSpeed(s_sScConfig[0].u8ColourfulDeviceSpeed);
}

static void iModeInc(void)
{
//    static uint32 s_u32LastModeIncTime = 0;
    
    if(s_sScConfig[0].eSwitch != E_SWITCH_STATUS_ON)
	{
		return;
	}

//    if(GulSystickCount - s_u32LastModeIncTime < 200)
//    {
//        return;
//    }
//    s_u32LastModeIncTime = GulSystickCount;
    
//    if((s_sScConfig[0].eWorkModeType != E_WORK_MODE_TYPE_SCENCE))
//    {
//        s_sScConfig[0].eWorkModeType = E_WORK_MODE_TYPE_SCENCE;
//        iRefreshAsConfig();
//        return;
//    }
    s_sScConfig[0].eWorkModeType = E_WORK_MODE_TYPE_SCENCE;
    s_sScConfig[0].eScence += 1; 
	if(s_sScConfig[0].eScence >= E_SCENCE_TYPE_MAX)   //已到达最大的模式
	{
		s_sScConfig[0].eScence = E_SCENCE_TYPE_AUTO;
        iRefreshAsConfig();
        return;
	}    
		
	iRefreshAsConfig();
}

static void iModeDec(void)
{
    static uint32 s_u32LastModeDecTime = 0;
    
    if(s_sScConfig[0].eSwitch != E_SWITCH_STATUS_ON)
	{
		return;
	}

    if(GulSystickCount - s_u32LastModeDecTime < 200)
    {
        return;
    }
    s_u32LastModeDecTime = GulSystickCount;
    	
    if((s_sScConfig[0].eWorkModeType != E_WORK_MODE_TYPE_SCENCE))
    {
        s_sScConfig[0].eWorkModeType = E_WORK_MODE_TYPE_SCENCE;
        iRefreshAsConfig();
        return;
    }
    
	if(s_sScConfig[0].eScence == E_SCENCE_TYPE_AUTO)   //已到达最小的模式
	{
		s_sScConfig[0].eScence = E_SCENCE_TYPE_MAX - 1;
        iRefreshAsConfig();
        return;
	}
    
    s_sScConfig[0].eScence -= 1;
			
	iRefreshAsConfig();
}

//static void iColorInc(void)
//{
//    uint16 u16H = 0, u16S = 0, u16V = 0;
//    
//    if(s_sScConfig[0].eSwitch != E_SWITCH_STATUS_ON)
//	{
//		return;
//	}
//    
//    if(s_sScConfig[0].eWorkModeType != E_WORK_MODE_TYPE_COLOUR)
//    {
//        s_sScConfig[0].eWorkModeType = E_WORK_MODE_TYPE_COLOUR;
//        iRefreshAsConfig();
//        return;
//    }
//    
//    RgbToHsv(&u16H, &u16S, &u16V, s_sScConfig[0].u8ColourfulDeviceaRgb[R_INDEX], s_sScConfig[0].u8ColourfulDeviceaRgb[G_INDEX], s_sScConfig[0].u8ColourfulDeviceaRgb[B_INDEX]);
//    if((u16H < 120) && (u16H + APS_H_GRID > 120))
//    {
//        u16H = 120;
//    }
//    else if((u16H < 240) && (u16H + APS_H_GRID > 240))
//    {
//        u16H = 240;
//    }
//    else if((u16H < 360) && (u16H + APS_H_GRID >= 360))
//    {
//        u16H = 0;
//    }
//    else
//    {
//        u16H += APS_H_GRID;
//    }
//            
//    HsvToRgb(u16H, S_MAX, V_MAX, &s_sScConfig[0].u8ColourfulDeviceaRgb[R_INDEX], &s_sScConfig[0].u8ColourfulDeviceaRgb[G_INDEX], &s_sScConfig[0].u8ColourfulDeviceaRgb[B_INDEX]);
//    
//    iRefreshAsConfig();
//}

//static void iColorDec(void)
//{
//    uint16 u16H = 0, u16S = 0, u16V = 0;
//    
//    if(s_sScConfig[0].eSwitch != E_SWITCH_STATUS_ON)
//	{
//		return;
//	}
//    
//    if(s_sScConfig[0].eWorkModeType != E_WORK_MODE_TYPE_COLOUR)
//    {
//        s_sScConfig[0].eWorkModeType = E_WORK_MODE_TYPE_COLOUR;
//        iRefreshAsConfig();
//        return;
//    }
//    
//    RgbToHsv(&u16H, &u16S, &u16V, s_sScConfig[0].u8ColourfulDeviceaRgb[R_INDEX], s_sScConfig[0].u8ColourfulDeviceaRgb[G_INDEX], s_sScConfig[0].u8ColourfulDeviceaRgb[B_INDEX]);
//        
//    if(u16H == 0)
//    {
//        u16H = 360;
//    }

//    if((u16H > 240) && (u16H - APS_H_GRID < 240))
//    {
//        u16H = 240;
//    }
//    else if((u16H > 120) && (u16H - APS_H_GRID < 120))
//    {
//        u16H = 120;
//    }
//    else if(u16H < APS_H_GRID)
//    {
//        u16H = 0;
//    }0
//    else
//    {
//        u16H -= APS_H_GRID;
//    }
//               
//    HsvToRgb(u16H, S_MAX, V_MAX, &s_sScConfig[0].u8ColourfulDeviceaRgb[R_INDEX], &s_sScConfig[0].u8ColourfulDeviceaRgb[G_INDEX], &s_sScConfig[0].u8ColourfulDeviceaRgb[B_INDEX]);
//    
//    iRefreshAsConfig();
//}

static void iColorSet(uint8 u8H)
{
    int16 s16h = 0;
    
    if(s_sScConfig[0].eSwitch != E_SWITCH_STATUS_ON)
	{
		return;
	}
    
    s16h = (int16)(((float)u8H) /  SEHUAN_VALUE_MAX * H_MAX);
            
    if(((s16h > 0) && (s16h < 10)) || ((s16h > 350) && (s16h < 360)))
    {
       s16h = 0;
    }
    else if((s16h > 110) && (s16h < 130))
    {
       s16h = 120;
    }
    else if((s16h > 225) && (s16h < 255))
    {
       s16h = 240;
    }
            
    HsvToRgb(s16h, S_MAX, V_MAX, &s_sScConfig[0].u8ColourfulDeviceaRgb[R_INDEX], &s_sScConfig[0].u8ColourfulDeviceaRgb[G_INDEX], &s_sScConfig[0].u8ColourfulDeviceaRgb[B_INDEX]);
    s_sScConfig[0].eWorkModeType = E_WORK_MODE_TYPE_COLOUR;
    
	GLHLM_StaticMode(s_sScConfig[0].u8ColourfulDeviceaRgb[R_INDEX], s_sScConfig[0].u8ColourfulDeviceaRgb[G_INDEX], s_sScConfig[0].u8ColourfulDeviceaRgb[B_INDEX], 0, 0);
}

//static void iCwSet(uint8 u8Cw)
//{
//    uint8 u8CwTemp = 0;
//    
//    if(s_sScConfig[0].eSwitch != E_SWITCH_STATUS_ON)
//	{
//		return;
//	}
//    
//    if(u8Cw > APS_CW_MAX)
//    {
//        return;
//    }
//    
//    u8CwTemp = (float)u8Cw / APS_CW_MAX * COLOUR_MAX;
//    s_sScConfig[0].u8ColourTemprature = u8CwTemp;
//    s_sScConfig[0].eWorkModeType = E_WORK_MODE_TYPE_WHITE;
//    GLHLM_StaticMode(0, 0, 0, COLOUR_MAX - s_sScConfig[0].u8ColourTemprature, s_sScConfig[0].u8ColourTemprature);
//}

//static void iCwInc(void)
//{
//    if(s_sScConfig[0].eSwitch != E_SWITCH_STATUS_ON)
//	{
//		return;
//	}
//    
//    if(s_sScConfig[0].eWorkModeType != E_WORK_MODE_TYPE_WHITE)
//    {
//        s_sScConfig[0].eWorkModeType = E_WORK_MODE_TYPE_WHITE;
//        GLHLM_StaticMode(0, 0, 0, COLOUR_MAX - s_sScConfig[0].u8ColourTemprature, s_sScConfig[0].u8ColourTemprature);
//        return;
//    }
//    
//    if(s_sScConfig[0].u8ColourTemprature == COLOUR_MAX)
//    {
//        return;
//    }
//    
//    if(s_sScConfig[0].u8ColourTemprature + APS_CW_CHANGE < COLOUR_MAX)
//    {
//        s_sScConfig[0].u8ColourTemprature += APS_CW_CHANGE;
//    }
//    else
//    {
//        s_sScConfig[0].u8ColourTemprature = COLOUR_MAX;
//    }
//    
//    GLHLM_StaticMode(0, 0, 0, COLOUR_MAX - s_sScConfig[0].u8ColourTemprature, s_sScConfig[0].u8ColourTemprature);
//}

//static void iCwDec(void)
//{
//    if(s_sScConfig[0].eSwitch != E_SWITCH_STATUS_ON)
//	{
//		return;
//	}

//    if(s_sScConfig[0].eWorkModeType != E_WORK_MODE_TYPE_WHITE)
//    {
//        s_sScConfig[0].eWorkModeType = E_WORK_MODE_TYPE_WHITE;
//        GLHLM_StaticMode(0, 0, 0, COLOUR_MAX - s_sScConfig[0].u8ColourTemprature, s_sScConfig[0].u8ColourTemprature);
//        return;
//    }
//    
//    if(s_sScConfig[0].u8ColourTemprature == 0)
//    {
//        return;
//    }
//    
//    if(s_sScConfig[0].u8ColourTemprature > APS_CW_CHANGE)
//    {
//        s_sScConfig[0].u8ColourTemprature -= APS_CW_CHANGE;
//    }
//    else
//    {
//        s_sScConfig[0].u8ColourTemprature = 0;
//    }
//    
//    GLHLM_StaticMode(0, 0, 0, COLOUR_MAX - s_sScConfig[0].u8ColourTemprature, s_sScConfig[0].u8ColourTemprature);
//}

static void iSaveScence(uint8 u8SenceId)
{
    uint32 u32WaitStartTime = 0;
    uint8 i = 0;
    
    if(u8SenceId > SC_CONFIG_BUFF_NUM - 1)
    {
        return;
    }
    
    if(s_sScConfig[0].eSwitch != E_SWITCH_STATUS_ON)
    {
		return;
    }
    
    if(s_sScConfig[0].u8ColourfulDeviceBn == s_sScConfig[u8SenceId].u8ColourfulDeviceBn)
    {
        return;
    }
    
    memcpy(&s_sScConfig[u8SenceId], &s_sScConfig[0], sizeof(SC_CONFIG_S));

    for(i=0; i<3; i++)
    {
        GLHPWM_Set(PWM_CHANNEL_R, ((uint32)GLHPWM_MAX) >> 2);
        GLHPWM_Set(PWM_CHANNEL_G, ((uint32)GLHPWM_MAX) >> 2);
        GLHPWM_Set(PWM_CHANNEL_B, ((uint32)GLHPWM_MAX) >> 2);
        GLHPWM_Set(PWM_CHANNEL_C, ((uint32)GLHPWM_MAX) >> 2);
        GLHPWM_Set(PWM_CHANNEL_W, ((uint32)GLHPWM_MAX) >> 2);
        
        u32WaitStartTime = GulSystickCount;
        while(GulSystickCount - u32WaitStartTime < 100)
        {
            GLHWD_Clear();
        }
        
        GLHPWM_Set(PWM_CHANNEL_R, 0);
        GLHPWM_Set(PWM_CHANNEL_G, 0);
        GLHPWM_Set(PWM_CHANNEL_B, 0);
        GLHPWM_Set(PWM_CHANNEL_C, 0);
        GLHPWM_Set(PWM_CHANNEL_W, 0);
        
        u32WaitStartTime = GulSystickCount;
        while(GulSystickCount - u32WaitStartTime < 100)
        {
            GLHWD_Clear();
        }
    }
    
    GLHLM_SetBn(s_sScConfig[0].u8ColourfulDeviceBn);    //这里设置亮度，只是为了恢复原本显示的颜色。
}

static void iExcuteScence(uint8 u8SenceId)
{
	uint32 au32AddrTemp[ADDR_NUM_MAX] = {0};         
	uint8 au8GroupTemp[ADDR_NUM_MAX] = {0}; 
    
    if(u8SenceId > SC_CONFIG_BUFF_NUM - 1)
    {
        return;
    }
    
    if(s_sScConfig[0].eSwitch != E_SWITCH_STATUS_ON)
    {
		return;
	}
    
    memcpy((uint8 *)au32AddrTemp, (uint8 *)s_sScConfig[0].au32Addr, sizeof(au32AddrTemp));
    memcpy((uint8 *)au8GroupTemp, (uint8 *)s_sScConfig[0].au8Group, sizeof(au8GroupTemp));
    
    memcpy(&s_sScConfig[0], &s_sScConfig[u8SenceId], sizeof(SC_CONFIG_S));
    
    memcpy((uint8 *)s_sScConfig[0].au32Addr, (uint8 *)au32AddrTemp, sizeof(au32AddrTemp));
    memcpy((uint8 *)s_sScConfig[0].au8Group, (uint8 *)au8GroupTemp, sizeof(au8GroupTemp));
    
    iRefreshAsConfig();
}

static void iSetTimingPowerOff(uint16 u16Time)
{
    uint8 i = 0;
    uint32 u32WaitStartTime = 0;
    
    if(s_sScConfig[0].eSwitch == E_SWITCH_STATUS_OFF)
	{
		return;
	}
    
    s_bTimingPowerOffFlag = TRUE;
    s_u32TimingStartTime = GulSystickCount;
    s_u32TimingTime = u16Time*2*1000;
    
    for(i=0; i<1; i++)
    {
        GLHPWM_Set(PWM_CHANNEL_R, ((uint32)GLHPWM_MAX) >> 2);
        GLHPWM_Set(PWM_CHANNEL_G, ((uint32)GLHPWM_MAX) >> 2);
        GLHPWM_Set(PWM_CHANNEL_B, ((uint32)GLHPWM_MAX) >> 2);
        GLHPWM_Set(PWM_CHANNEL_C, 0);
        GLHPWM_Set(PWM_CHANNEL_W, 0);
        
        u32WaitStartTime = GulSystickCount;
        while(GulSystickCount - u32WaitStartTime < 200)
        {
            GLHWD_Clear();
        }
        
        GLHPWM_Set(PWM_CHANNEL_R, 0);
        GLHPWM_Set(PWM_CHANNEL_G, 0);
        GLHPWM_Set(PWM_CHANNEL_B, 0);
        GLHPWM_Set(PWM_CHANNEL_C, 0);
        GLHPWM_Set(PWM_CHANNEL_W, 0);
        
        u32WaitStartTime = GulSystickCount;
        while(GulSystickCount - u32WaitStartTime < 200)
        {
            GLHWD_Clear();
        }
    }

    GLHLM_SetBn(s_sScConfig[0].u8ColourfulDeviceBn);    //这里设置亮度，只是为了恢复原本显示的颜色。
}

static void iTimingPowerOffCancel(void)
{
    s_bTimingPowerOffFlag = FALSE;
}

static void iTimingPowerOffMainThread(void)
{
    if(s_bTimingPowerOffFlag != TRUE)
    {
        return;
    }
    
    if(GulSystickCount - s_u32TimingStartTime >= s_u32TimingTime)
    {
        s_bTimingPowerOffFlag = FALSE;
        s_sScConfig[0].eSwitch = E_SWITCH_STATUS_OFF;
        iRefreshAsConfig();
    }
}

//参数入参范围h(0~360),s(0~S_MAX),v(0~V_MAX),这里要注意，要把s,v缩放到0~1之间
//转换结果R(0~COLOUR_MAX),G(0~COLOUR_MAX),B(0~COLOUR_MAX).
void HsvToRgb(int16 h, int16 s, int16 v, uint8 *pu8R, uint8 *pu8G, uint8 *pu8B)
{
    float C = 0,X = 0,Y = 0,Z = 0, R = 0, G = 0, B = 0;
    int16 i=0;
    float H=(float)(h);
    float S=(float)(s)/S_MAX;
    float V=(float)(v)/V_MAX;
    
    if(S == 0)
    {
        R = G = B = V;
    }
    else
    {
        H = H/60;
        i = (int16)H;
        C = H - i;
 
        X = V * (1 - S);
        Y = V * (1 - S*C);
        Z = V * (1 - S*(1-C));
        switch(i)
        {
            case 0 : R = V; G = Z; B = X; break;
            case 1 : R = Y; G = V; B = X; break;
            case 2 : R = X; G = V; B = Z; break;
            case 3 : R = X; G = Y; B = V; break;
            case 4 : R = Z; G = X; B = V; break;
            case 5 : R = V; G = X; B = Y; break;
        }
    }
    
    *pu8R = R * COLOUR_MAX;
    *pu8G = G * COLOUR_MAX;
    *pu8B = B * COLOUR_MAX;
}

float RetMax(float a,float b,float c)//求最大值
{
    float max = 0;
    max = a;
    if(max<b)
        max = b;
    if(max<c)
        max = c;
    return max;
}
float RetMin(float a,float b,float c)//求最小值
{
    float min = 0;
    min = a;
    if(min>b)
        min = b;
    if(min>c)
        min = c;
    return min;
}
//R,G,B参数传入范围（0~COLOUR_MAX）
//转换结果h(0~360),s(0~S_MAX),v(0~V_MAX)
void RgbToHsv(uint16 *pu16H, uint16 *pu16S, uint16 *pu16V, uint8 u8R, uint8 u8G, uint8 u8B)
{
    float fMax = 0,fMin = 0;
    float h = 0, s = 0, v = 0, R = 0, G = 0, B = 0;
    
    R = u8R;
    G = u8G;
    B = u8B;
    
    R = R/COLOUR_MAX;
    G = G/COLOUR_MAX;
    B = B/COLOUR_MAX;
 
    fMax = RetMax(R,G,B);
    fMin = RetMin(R,G,B);
    v = fMax;
    if(fMax == 0)
        s = 0;
    else
        s = 1 - (fMin/fMax);
 
    if(fMax == fMin)
    {
        h = 0;
    }
    else if(fMax == R && G>=B)
    {
        h = 60*((G-B)/(fMax-fMin));
    }
    else if(fMax == R && G<B)
    {
        h = 60*((G-B)/(fMax-fMin)) + 360;
    }
    else if(fMax == G)
    {
        h = 60*((B-R)/(fMax-fMin)) + 120;
    }
    else if(fMax == B)
    {
        h = 60*((R-G)/(fMax-fMin)) + 240;
    }
 
    v = v * V_MAX;
    s = s * S_MAX;
    
    *pu16H = h;
    *pu16S = s;
    *pu16V = v;
}

unsigned char MyCrol(unsigned char u8Byte, unsigned char u8Cnt)
{
    unsigned char u8Temp = 0, i = 0;
    unsigned char u8Value = u8Byte;
    
    for(i=0; i<u8Cnt; i++)
    {
        u8Temp = ((u8Value & 0x80) >> 7);
        u8Value = (u8Value << 1) | u8Temp;
    }
    
    return u8Value;
}

void DataDecryptionProcessing(unsigned char *u8pBuff, unsigned char u8Len)
{
    unsigned char i = 0;
    
    if(u8pBuff == 0)
    {
        return;
    }
    
    if(u8Len == 0)
    {
        return;
    }

    u8pBuff[RANDOM_INDEX] = MyCrol(u8pBuff[RANDOM_INDEX], 2);
    
    u8pBuff[0] ^= 0x20;
    u8pBuff[1] ^= 0x22;
    u8pBuff[2] ^= 0x05;
    u8pBuff[3] ^= 0x21;
    
    for(i=0; i<u8Len; i++)
    {
        if(i != RANDOM_INDEX)
        {
            u8pBuff[i] ^= u8pBuff[RANDOM_INDEX];
        }
    }
}

uint8 RemoterDataTemp[REMOTER_BUFF_LEN] = {0};
static void iRf24gReceiveMainThread(void)
{
    RF24G_REC_RESULT_E eRf24gRecResult = E_RF24G_REC_STATUS_UNKNOWN;
    
    eRf24gRecResult = RF24G_IsTxOrRxComplete();
    if(eRf24gRecResult != E_RF24G_REC_STATUS_NO)
    {
        if(eRf24gRecResult == E_RF24G_REC_STATUS_OK)
        {
            RF24G_ReadRxData(s_au8Rf24gRecBuff, &u8Rf24gRecLen);             //从fifo中读取接收到的数据
            if(u8Rf24gRecLen == REMOTER_BUFF_LEN)
            {
                memcpy(RemoterDataTemp, s_au8Rf24gRecBuff, REMOTER_BUFF_LEN);     //解密数据之前，保存原始数据
                DataDecryptionProcessing(s_au8Rf24gRecBuff, u8Rf24gRecLen);  //解密数据
                iRf24gRemoterDataHandle(s_au8Rf24gRecBuff, &u8Rf24gRecLen);  //遥控器数据处理
            }
            else if(u8Rf24gRecLen == SYNC_BUFF_LEN)                             
            {
                iRf24gSyncDataHandle(s_au8Rf24gRecBuff, &u8Rf24gRecLen);     //同步数据处理
            }
        
            s_u8Channel++;
            if(s_u8Channel >= RF24G_CHANNEL_NUM)
            {
                s_u8Channel = 0;
            }
            RF24G_EnterRxMode(RF24G_CH);
            
            s_u32LastChannelChangeTime = GulSystickCount;
        }
        else if(eRf24gRecResult == E_RF24G_REC_STATUS_ERROR)
        {
            s_u8Channel++;
            if(s_u8Channel >= RF24G_CHANNEL_NUM)
            {
                s_u8Channel = 0;
            }
            RF24G_EnterRxMode(RF24G_CH);
            
            s_u32LastChannelChangeTime = GulSystickCount;
        }
    }
    else
    {
        if(GulSystickCount - s_u32LastChannelChangeTime >= 7)
        {
            s_u32LastChannelChangeTime = GulSystickCount;
        
            s_u8Channel++;
            if(s_u8Channel >= RF24G_CHANNEL_NUM)
            {
                s_u8Channel = 0;
            }
        
            RF24G_EnterRxMode(RF24G_CH);
        }    
    }
}

static void iRf24gChannelChangeMainThread(void)
{   
//    if(GulSystickCount - s_u32LastChannelChangeTime > 7)
//    {
//        s_u32LastChannelChangeTime = GulSystickCount;
//        
//        s_u8Channel++;
//        if(s_u8Channel >= RF24G_CHANNEL_NUM)
//        {
//            s_u8Channel = 0;
//        }
//        
//        RF24G_EnterRxMode(0x4a);
//    }    
}

//unsigned char GetCheckSum(unsigned char *u8pBuff, unsigned char u8Len)
//{
//    unsigned char u8CheckSum = 0, i = 0;
//    
//    for(i=0; i<u8Len; i++)
//    {
//        u8CheckSum += u8pBuff[i];
//    }
//    
//    return u8CheckSum;
//}

static uint32 s_u32LastModeTime = 0;
static void iRf24gSyncDataHandle(uint8 *pu8RxData, uint8 *pu8RxLen)
{
    uint8 i = 0;
    uint8 u8CheckSum = 0;
    uint32 u32CurrAddr = 0;
    uint8 u8CurrGroup = 0;
    static uint32 s_u8LastRecPackageNum = 0;

    if(pu8RxData[TX_DEVICE_TYPE_INDEX] != s_sScConfig[0].eDeviceType)
    {
        return;
    }
    
    if(s_sScConfig[0].eSwitch != E_SWITCH_STATUS_ON)
    {
        return;
    }
    
    if(*pu8RxLen != SYNC_BUFF_LEN)
    {
        return;
    }
    
    for(i=0; i<((*pu8RxLen) - 1); i++)
    {
        u8CheckSum += pu8RxData[i];
    }
    
    if(u8CheckSum != pu8RxData[TX_CHECK_INDEX])
    {
        return;
    }
    
    if(s_u8LastRecPackageNum == pu8RxData[TX_PACK_NUM_INDEX])
    {
        return;
    }
    s_u8LastRecPackageNum = pu8RxData[TX_PACK_NUM_INDEX];
    s_u8PackNum = pu8RxData[TX_PACK_NUM_INDEX];
    
    u32CurrAddr = (((uint32)pu8RxData[TX_ADDR0_INDEX]) << 24) | (((uint32)pu8RxData[TX_ADDR1_INDEX]) << 16) |\
                  (((uint32)pu8RxData[TX_ADDR2_INDEX]) << 8) | (((uint32)pu8RxData[TX_ADDR3_INDEX]) << 0);
    
    u8CurrGroup = pu8RxData[TX_GROUP_INDEX];

    if(s_sScConfig[0].u32CurrAddr == EMPTY_ADDR)
    {
        return;
    }
        
    if((s_sScConfig[0].u32CurrAddr != u32CurrAddr) || (s_sScConfig[0].u8CurrGroup != u8CurrGroup))
    {
        return;
    }
    
    s_u32LastSyncTime = GulSystickCount;
    
    if(pu8RxData[TX_MODE_FLAG_INDEX] != FALSE)
    {
        s_u32LastModeTime = GulSystickCount;
    }
    
    if(pu8RxData[TX_STEP_INDEX] == 0)   //每一个模式最开始的时候
    {
        s_sScConfig[0].eSwitch = pu8RxData[TX_SWITCH_INDEX];
        s_sScConfig[0].u8ColourfulDeviceBn = pu8RxData[TX_BN_INDEX];
        s_sScConfig[0].u8ColourfulDeviceSpeed = pu8RxData[TX_SPEED_INDEX];
        s_sScConfig[0].u8ColourfulDeviceaRgb[0] = pu8RxData[TX_R_INDEX];
        s_sScConfig[0].u8ColourfulDeviceaRgb[1] = pu8RxData[TX_G_INDEX];
        s_sScConfig[0].u8ColourfulDeviceaRgb[2] = pu8RxData[TX_B_INDEX];
        s_sScConfig[0].u8ColourfulDeviceaRgb[3] = pu8RxData[TX_W_INDEX];
        s_sScConfig[0].eScence = pu8RxData[TX_SCENCE_INDEX];
        s_sScConfig[0].eWorkModeType = pu8RxData[TX_MODE_INDEX];
        s_eModeAuto = pu8RxData[TX_AUTO_MODE_INDEX];
    
        iRefreshAsConfig();
        if((s_sScConfig[0].eWorkModeType == E_WORK_MODE_TYPE_SCENCE) && (s_sScConfig[0].eScence == E_SCENCE_TYPE_AUTO))
        {
            s_eModeAuto = pu8RxData[TX_AUTO_MODE_INDEX];
            iSlectScence(s_eModeAuto);
        }
        GLHLM_SetCurrRgbw(pu8RxData[TX_CURRENT_R_INDEX], pu8RxData[TX_CURRENT_G_INDEX], pu8RxData[TX_CURRENT_B_INDEX], pu8RxData[TX_CURRENT_W_INDEX]);
    }
    else
    {
        if((s_sScConfig[0].eScence != pu8RxData[TX_SCENCE_INDEX]) || (s_sScConfig[0].eWorkModeType != pu8RxData[TX_MODE_INDEX]))  
        {
            s_sScConfig[0].eSwitch = pu8RxData[TX_SWITCH_INDEX];
            s_sScConfig[0].u8ColourfulDeviceBn = pu8RxData[TX_BN_INDEX];
            s_sScConfig[0].u8ColourfulDeviceSpeed = pu8RxData[TX_SPEED_INDEX];
            s_sScConfig[0].u8ColourfulDeviceaRgb[0] = pu8RxData[TX_R_INDEX];
            s_sScConfig[0].u8ColourfulDeviceaRgb[1] = pu8RxData[TX_G_INDEX];
            s_sScConfig[0].u8ColourfulDeviceaRgb[2] = pu8RxData[TX_B_INDEX];
            s_sScConfig[0].u8ColourfulDeviceaRgb[3] = pu8RxData[TX_W_INDEX];
            s_sScConfig[0].eScence = pu8RxData[TX_SCENCE_INDEX];
            s_sScConfig[0].eWorkModeType = pu8RxData[TX_MODE_INDEX];
            s_eModeAuto = pu8RxData[TX_AUTO_MODE_INDEX];
    
            iRefreshAsConfig();
            if((s_sScConfig[0].eWorkModeType == E_WORK_MODE_TYPE_SCENCE) && (s_sScConfig[0].eScence == E_SCENCE_TYPE_AUTO))
            {
                s_eModeAuto = pu8RxData[TX_AUTO_MODE_INDEX];
                iSlectScence(s_eModeAuto);
            }
            GLHLM_SetCurrRgbw(pu8RxData[TX_CURRENT_R_INDEX], pu8RxData[TX_CURRENT_G_INDEX], pu8RxData[TX_CURRENT_B_INDEX], pu8RxData[TX_CURRENT_W_INDEX]);
        }
        GLHLM_SetCurrRgbw(pu8RxData[TX_CURRENT_R_INDEX], pu8RxData[TX_CURRENT_G_INDEX], pu8RxData[TX_CURRENT_B_INDEX], pu8RxData[TX_CURRENT_W_INDEX]);
        GLHLM_SetCurrColorIndex(pu8RxData[TX_U8INDEX_INDEX]);
        GLHLM_SetStep(pu8RxData[TX_STEP_INDEX]);
    }
}

static void iRf24gRemoterDataHandle(uint8 *pu8RxData, uint8 *pu8RxLen)
{
    static uint8 s_u8LastPackgeNum = 0x55;
    uint32 u32Addr = 0;
    uint8 i = 0, j = 0;
    uint8 u8Temp = 0;
    DEVICE_TYPE_E eDeviceType = E_DEVICE_TYPE_UNKNOWN;
    
    if(*pu8RxLen != 13)    //协议中数据长度为13
    {
        return;
    }
    
    if(pu8RxData[GROUP_NUM_INDEX] > 4)
    {
        return;
    }
    
    if(s_u8LastPackgeNum == pu8RxData[PACK_NUM_INDEX])
    {
        return;
    }
    s_u8LastPackgeNum = pu8RxData[PACK_NUM_INDEX];
    
    u8Temp = pu8RxData[IDENTIFICATION_CODE_INDEX] >> 4;
    if((u8Temp != 1) && (u8Temp != 3)&& (u8Temp != 4))   //不是CW,RGB,RGBW设备类型中的一种，不做处理
    {
        return;
    }
    
    switch(u8Temp)
    {
#ifdef SUPPORT_DEVIVE_TYPE_DIMMER
        case 1:
        {
            eDeviceType = E_DEVICE_TYPE_DIMMER;
        }
        break;
#endif

#ifdef SUPPORT_DEVIVE_TYPE_RGB        
        case 3:
        {
            eDeviceType = E_DEVICE_TYPE_RGB;
        }
        break;
#endif

#ifdef SUPPORT_DEVIVE_TYPE_RGBW        
        case 4:
        {
            eDeviceType = E_DEVICE_TYPE_RGBW;
        }
        break;
#endif
    }
    
    if(eDeviceType != s_sScConfig[0].eDeviceType)
    {
        return;
    }
    
    u32Addr = ((uint32)pu8RxData[ADDR1_INDEX] << 16) | (uint32)(pu8RxData[ADDR2_INDEX] << 8) | (uint32)pu8RxData[ADDR3_INDEX];
    
//	if(s_sScConfig[0].bIsNeedPair == TRUE)  //需要配对
//	{
		if(GulSystickCount < 8000)      //开机时间在8s以内
		{
			if((((pu8RxData[DATA_POINT_INDEX] & 0x7f) == DPID_SWITCH)) && (pu8RxData[GROUP_NUM_INDEX] != GROUP_ALL))
			{
				if(s_bPairFlag == TRUE)   //本次上电已经因 E_BEACON_COM_TYPE_SPEED_INC_LONG 执行过一次配对或清码的动作
				{
                    goto ADDR_CHECK;
				}
                else
                {
                    if(pu8RxData[DATA_POINT_INDEX] & 0x80)   //按键长按
                    {
                        for(i=0; i<ADDR_NUM_MAX; i++)
                        {
                            if((u32Addr == s_sScConfig[0].au32Addr[i]) && (pu8RxData[GROUP_NUM_INDEX] == s_sScConfig[0].au8Group[i]))     //学习过接收到的ADDR
                            {
                                if(i < (ADDR_NUM_MAX-1))
                                {
                                    memcpy(&s_sScConfig[0].au32Addr[i], &s_sScConfig[0].au32Addr[i+1], (ADDR_NUM_MAX-1-i)*sizeof(s_sScConfig[0].au32Addr[0]));   //清码
                                    s_sScConfig[0].au32Addr[ADDR_NUM_MAX-1] = EMPTY_ADDR;
                                    
                                    memcpy(&s_sScConfig[0].au8Group[i], &s_sScConfig[0].au8Group[i+1], (ADDR_NUM_MAX-1-i)*sizeof(s_sScConfig[0].au8Group[0]));    //清分组号
                                    s_sScConfig[0].au8Group[ADDR_NUM_MAX-1] = EMPTY_GROUP;
                                    
                                    iRgbflash(9, 150);    //快闪9次，提示清码成功
                                    
                                    if(s_sScConfig[0].u8PairNum > 0)
                                    {
                                        s_sScConfig[0].u8PairNum--;
                                        
                                        if(s_sScConfig[0].u8PairNum == 0)
                                        {
//                                            s_sScConfig[0].bIsNeedPair = FALSE;
                                        }
                                    }
                                    
                                    s_bPairFlag = TRUE;
                                    
                                    s_sScConfig[0].eSwitch = E_SWITCH_STATUS_ON;
                                    iRefreshAsConfig();                                    
                                    
                                    return;
                                }
                                else                      //接收到的ADDR存储在BUFF最后，直接清除
                                {
                                    s_sScConfig[0].au32Addr[ADDR_NUM_MAX-1] = EMPTY_ADDR;
                                    s_sScConfig[0].au8Group[ADDR_NUM_MAX-1] = EMPTY_GROUP;
                                    
                                    iRgbflash(9, 150);    //快闪9次，提示清码成功
                                    
                                    if(s_sScConfig[0].u8PairNum > 0)
                                    {
                                        s_sScConfig[0].u8PairNum--;
                                        
                                        if(s_sScConfig[0].u8PairNum == 0)
                                        {
//                                            s_sScConfig[0].bIsNeedPair = FALSE;
                                        }
                                    }
                                    
                                    s_bPairFlag = TRUE;

                                    s_sScConfig[0].eSwitch = E_SWITCH_STATUS_ON;
                                    iRefreshAsConfig();
                                    
                                    return;
                                }
                            }
                        }
                        
                        if(i >= ADDR_NUM_MAX)    //前面查询完，没有发现配对过，需要配对
                        {
                            for(i=0; i<ADDR_NUM_MAX; i++)
                            {
                                if((s_sScConfig[0].au32Addr[i] == EMPTY_ADDR) && (s_sScConfig[0].au8Group[i] == EMPTY_GROUP))    //查询哪个位置是空的可以直接存放现在配对的地址号码
                                {
                                    s_sScConfig[0].au32Addr[i] = u32Addr;
                                    s_sScConfig[0].au8Group[i] = pu8RxData[GROUP_NUM_INDEX];
                                    iRgbflash(3, 500);    //慢闪3次，提示配对成功

                                    if(eDeviceType != s_sScConfig[0].eDeviceType)
                                    {
                                        s_sScConfig[0].eDeviceType = eDeviceType;
                                        s_sScConfig[0].eSwitch = E_SWITCH_STATUS_ON;
                                        s_sScConfig[0].u8ColourfulDeviceBn = APS_BN_MAX;
                                        s_sScConfig[0].u8ColourfulDeviceSpeed = APS_DEFAULT_SPEED;
                                        s_sScConfig[0].u8ColourfulDeviceaRgb[R_INDEX] = COLOUR_MAX;
                                        s_sScConfig[0].u8ColourfulDeviceaRgb[G_INDEX] = 0;
                                        s_sScConfig[0].u8ColourfulDeviceaRgb[B_INDEX] = 0;
                                        s_sScConfig[0].eWorkModeType = E_WORK_MODE_TYPE_COLOUR;
                                        s_sScConfig[0].eScence = E_SCENCE_TYPE_JUMP_RGB;
                                        iRefreshAsConfig();
                                    }
                                
                                    s_sScConfig[0].u8PairNum++;
                                    if(s_sScConfig[0].u8PairNum > ADDR_NUM_MAX)
                                    {
                                        s_sScConfig[0].u8PairNum = ADDR_NUM_MAX;
                                    }
                                    s_sScConfig[0].bIsNeedPair = TRUE;
                                    
                                    s_bPairFlag = TRUE;
                                    
                                    s_sScConfig[0].eSwitch = E_SWITCH_STATUS_ON;
                                    iRefreshAsConfig();

                                    s_sScConfig[0].eSwitch = E_SWITCH_STATUS_ON;
                                    iRefreshAsConfig();
                                    
                                    return;
                                }
                            }
                            
                            if(i >= ADDR_NUM_MAX)  //没有查询到有空位置可以直接存放
                            {
                                memcpy(&s_sScConfig[0].au32Addr[0], &s_sScConfig[0].au32Addr[1], (ADDR_NUM_MAX-1)*sizeof(s_sScConfig[0].au32Addr[0]));   
                                s_sScConfig[0].au32Addr[ADDR_NUM_MAX-1] = u32Addr;
                                    
                                memcpy(&s_sScConfig[0].au8Group[0], &s_sScConfig[0].au8Group[1], (ADDR_NUM_MAX-1)*sizeof(s_sScConfig[0].au8Group[0]));   
                                s_sScConfig[0].au8Group[ADDR_NUM_MAX-1] = pu8RxData[GROUP_NUM_INDEX];

                                iRgbflash(3, 500);    //慢闪3次，提示配对成功
                                
                                if(eDeviceType != s_sScConfig[0].eDeviceType)
                                {
                                    s_sScConfig[0].eDeviceType = eDeviceType;
                                    s_sScConfig[0].eSwitch = E_SWITCH_STATUS_ON;
                                    s_sScConfig[0].u8ColourfulDeviceBn = APS_BN_MAX;
                                    s_sScConfig[0].u8ColourfulDeviceSpeed = APS_DEFAULT_SPEED;
                                    s_sScConfig[0].u8ColourfulDeviceaRgb[R_INDEX] = COLOUR_MAX;
                                    s_sScConfig[0].u8ColourfulDeviceaRgb[G_INDEX] = 0;
                                    s_sScConfig[0].u8ColourfulDeviceaRgb[B_INDEX] = 0;
                                    s_sScConfig[0].eWorkModeType = E_WORK_MODE_TYPE_COLOUR;
                                    s_sScConfig[0].eScence = E_SCENCE_TYPE_JUMP_RGB;
                                    iRefreshAsConfig();
                                }
                                
                                s_sScConfig[0].u8PairNum++;
                                if(s_sScConfig[0].u8PairNum > ADDR_NUM_MAX)
                                {
                                    s_sScConfig[0].u8PairNum = ADDR_NUM_MAX;
                                }
                                s_sScConfig[0].bIsNeedPair = TRUE;
                                
                                s_bPairFlag = TRUE;
                                
                                s_sScConfig[0].eSwitch = E_SWITCH_STATUS_ON;
                                iRefreshAsConfig();                                
                                    
                                return;
                            }
                        }
                    }
                }
            }
		}
//    }

ADDR_CHECK:
	if(s_sScConfig[0].bIsNeedPair == TRUE)  //需要配对
	{
        for(i=0; i<ADDR_NUM_MAX; i++)
        {
            if((u32Addr == s_sScConfig[0].au32Addr[i]) && ((pu8RxData[GROUP_NUM_INDEX] == s_sScConfig[0].au8Group[i]) || (pu8RxData[GROUP_NUM_INDEX] == GROUP_ALL)))
            {
                s_sScConfig[0].u32CurrAddr = u32Addr;
                s_sScConfig[0].u8CurrGroup = pu8RxData[GROUP_NUM_INDEX];
                goto RECEIVE_HANDEL;
            }
        }
        
        return;
	}    

RECEIVE_HANDEL:
//    if(s_bIsRgbControledByPair)
//	{
//        if(((pu8RxData[DATA_POINT_INDEX] & 0x7f) != DPID_ON) && ((pu8RxData[DATA_POINT_INDEX] & 0x7f) != DPID_SINGLE_WHITE)) 
//        {
//            s_bIsRgbControledByPair = FALSE;
//            iRefreshAsConfig();
//        }
//	}
    
	
	if(((pu8RxData[DATA_POINT_INDEX] & 0x7f) != DPID_ON) && ((pu8RxData[DATA_POINT_INDEX] & 0x7f) != DPID_SWITCH))   //上电检测到了非配对的按键，取消配对或清码
	{
		s_bPairFlag = TRUE;  
	}
    
    if(eDeviceType != s_sScConfig[0].eDeviceType)
    {
        s_sScConfig[0].eDeviceType = eDeviceType;
        s_sScConfig[0].eSwitch = E_SWITCH_STATUS_ON;
        s_sScConfig[0].u8ColourfulDeviceBn = APS_BN_MAX;
        s_sScConfig[0].u8ColourfulDeviceSpeed = APS_DEFAULT_SPEED;
        s_sScConfig[0].u8ColourfulDeviceaRgb[R_INDEX] = COLOUR_MAX;
        s_sScConfig[0].u8ColourfulDeviceaRgb[G_INDEX] = 0;
        s_sScConfig[0].u8ColourfulDeviceaRgb[B_INDEX] = 0;
        s_sScConfig[0].u8ColourfulDeviceaRgb[W_INDEX] = 0;
        s_sScConfig[0].eWorkModeType = E_WORK_MODE_TYPE_COLOUR;
        s_sScConfig[0].eScence = E_SCENCE_TYPE_JUMP_RGB;
        iRefreshAsConfig();
        return;
    }
    
if(0)
{

}
#ifdef SUPPORT_DEVIVE_TYPE_DIMMER
else if(eDeviceType == E_DEVICE_TYPE_DIMMER)
{
    switch(0x7f & pu8RxData[DATA_POINT_INDEX])
    {
        case DPID_ON:
        {
            if(pu8RxData[DATA_POINT_INDEX] & 0x80)   //按键长按
            {
//                s_sScConfig[0].eWorkModeType = E_WORK_MODE_TYPE_WHITE;
//                iRefreshAsConfig();            
            }
            else
            {
                if(s_sScConfig[0].eSwitch != E_SWITCH_STATUS_ON)
                {
                    s_sScConfig[0].eSwitch = E_SWITCH_STATUS_ON;
                    iRefreshAsConfig();
                }
                
                for(i=0; i<5; i++)
                {
                    s_u8Channel++;
                    if(s_u8Channel >= RF24G_CHANNEL_NUM)
                    {
                        s_u8Channel = 0;
                    }
        
                    RF24G_Tx(RemoterDataTemp, REMOTER_BUFF_LEN, RF24G_CH);
                    while(RF24G_IsTxOrRxComplete() == E_RF24G_REC_STATUS_NO)
                    {
        
                    }
                    Delay_ms((GulSystickCount % 2) + 1);
                } 
                RF24G_EnterRxMode(RF24G_CH);
            }
        }
        break;
        
        case DPID_OFF:
        {
            if(pu8RxData[DATA_POINT_INDEX] & 0x80)   //按键长按
            {
//                s_sScConfig[0].eSwitch = E_SWITCH_STATUS_NIGHT_LIGHT;
//                iRefreshAsConfig();            
            }
            else
            {
                if(s_sScConfig[0].eSwitch != E_SWITCH_STATUS_OFF)
                {
                    s_bTimingPowerOffFlag = FALSE;  //主动关机的时候，取消定时关机  
                
                    s_sScConfig[0].eSwitch = E_SWITCH_STATUS_OFF;
                    iRefreshAsConfig();
                }
                
                for(i=0; i<20; i++)
                {
                    s_u8Channel++;
                    if(s_u8Channel >= RF24G_CHANNEL_NUM)
                    {
                        s_u8Channel = 0;
                    }
        
                    RF24G_Tx(RemoterDataTemp, REMOTER_BUFF_LEN, RF24G_CH);
                    while(RF24G_IsTxOrRxComplete() == E_RF24G_REC_STATUS_NO)
                    {
        
                    }
                    Delay_ms((GulSystickCount % 3) + 1);
                } 
                RF24G_EnterRxMode(RF24G_CH);
            }
        }
        break;
        
        case DPID_SWITCH:
        {
            if(pu8RxData[DATA_POINT_INDEX] & 0x80)   //按键长按
            {
                return;
            }
            
            if(s_sScConfig[0].eSwitch != E_SWITCH_STATUS_OFF)
            {
                s_sScConfig[0].eSwitch = E_SWITCH_STATUS_OFF;
                iTimingPowerOffCancel();
            }
            else if(s_sScConfig[0].eSwitch == E_SWITCH_STATUS_OFF)
            {
                s_sScConfig[0].eSwitch = E_SWITCH_STATUS_ON;
            }
            iRefreshAsConfig();
            
            for(i=0; i<20; i++)
            {
                s_u8Channel++;
                if(s_u8Channel >= RF24G_CHANNEL_NUM)
                {
                    s_u8Channel = 0;
                }
        
                RF24G_Tx(RemoterDataTemp, REMOTER_BUFF_LEN, RF24G_CH);
                while(RF24G_IsTxOrRxComplete() == E_RF24G_REC_STATUS_NO)
                {
        
                }
                Delay_ms((GulSystickCount % 3) + 1);
            } 
            RF24G_EnterRxMode(RF24G_CH);
        }
        break;
        
        case DPID_BRIGHTNESS_SET:
        {   
            iBnSet(pu8RxData[VALUE1_INDEX]);
        }
        break;

        case DPID_SCENCE_SAVE:
        {
            iSaveScence(pu8RxData[VALUE1_INDEX]);    
        }
        break;
        
        case DPID_SCENCE_EXCUTE:
        {
            iExcuteScence(pu8RxData[VALUE1_INDEX]);    //长按保存时虽然会发送SAVE数据，但最高位会被置1，函数参数检查时就不会通过，所以此处不做另外判断
        }
        break;

        case DPID_TIMING:
        {
            iSetTimingPowerOff((((uint16)pu8RxData[VALUE2_INDEX]) << 8) | pu8RxData[VALUE1_INDEX]);
        }
        break;        
    }
}
#endif

#ifdef SUPPORT_DEVIVE_TYPE_RGB
else if(eDeviceType == E_DEVICE_TYPE_RGB)
{
    switch(0x7f & pu8RxData[DATA_POINT_INDEX])
    {
//        case DPID_SWITCH:
//        {
//            if(pu8RxData[DATA_POINT_INDEX] & 0x80)   //按键长按
//            {
//                return;
//            }
//            
//            if(s_sScConfig[0].eSwitch != E_SWITCH_STATUS_OFF)
//            {
//                s_sScConfig[0].eSwitch = E_SWITCH_STATUS_OFF;
//            }
//            else if(s_sScConfig[0].eSwitch == E_SWITCH_STATUS_OFF)
//            {
//                s_sScConfig[0].eSwitch = E_SWITCH_STATUS_ON;
//            }
//            iRefreshAsConfig();
//        }
//        break;
        
        case DPID_ON:
        {
            if(pu8RxData[DATA_POINT_INDEX] & 0x80)   //按键长按
            {
                s_sScConfig[0].eWorkModeType = E_WORK_MODE_MIX_WHITE;
                iRefreshAsConfig();            
            }
            else
            {
                if(s_sScConfig[0].eSwitch != E_SWITCH_STATUS_ON)
                {
                    s_sScConfig[0].eSwitch = E_SWITCH_STATUS_ON;
                    iRefreshAsConfig();
                }
            }
        }
        break;
        
        case DPID_OFF:
        {
            if(pu8RxData[DATA_POINT_INDEX] & 0x80)   //按键长按
            {
                s_sScConfig[0].eSwitch = E_SWITCH_STATUS_NIGHT_LIGHT;
                iRefreshAsConfig();            
            }
            else
            {
                if(s_sScConfig[0].eSwitch != E_SWITCH_STATUS_OFF)
                {
                    s_bTimingPowerOffFlag = FALSE;  //主动关机的时候，取消定时关机  
                
                    s_sScConfig[0].eSwitch = E_SWITCH_STATUS_OFF;
                    iRefreshAsConfig();
                }
            }
        }
        break;
        
        case DPID_SPEED_INC:
        {
            iSpeedInc();
        }
        break;      

        case DPID_SPEED_DEC:
        {
            iSpeedDec();
        }
        break;       

        case DPID_MODE_INC:
        {
//            if(s_sScConfig[0].eWorkModeType == E_WORK_MODE_TYPE_COLOUR)
//            {
//                s_sScConfig[0].eWorkModeType = E_WORK_MODE_MIX_WHITE;
//                iRefreshAsConfig();
//                return;
//            }
            iModeInc();
        }
        break; 
    
        case DPID_MODE_DEC:
        {
            if(s_sScConfig[0].eWorkModeType == E_WORK_MODE_TYPE_COLOUR)
            {
                s_sScConfig[0].eWorkModeType = E_WORK_MODE_MIX_WHITE;
                iRefreshAsConfig();
                return;
            }
            iModeDec();
        }
        break; 

        case DPID_BRIGHTNESS_INC:
        {
            iBnInc();
        }
        break;      

        case DPID_BRIGHTNESS_DEC:
        {
            iBnDec();
        }
        break; 

//        case DPID_BRIGHTNESS_SET:
//        {   
//            iBnSet(pu8RxData[VALUE1_INDEX]);
//        }
//        break;          

//        case DPID_COLOUR_INC:
//        {
//            iColorInc();
//        }
//        break;
        
//        case DPID_COLOUR_DEC:
//        {
//            iColorDec();
//        }
//        break;
        
        case DPID_COLOUR_SET:
        {
            iColorSet(pu8RxData[VALUE1_INDEX]);
        }
        break;     

//        case DPID_CW_SET:
//        {
//            iCwSet(pu8RxData[VALUE1_INDEX]);
//        }
//        break;
        
//        case DPID_CW_INC:
//        {
//            iCwInc();
//        }
//        break;
        
//        case DPID_CW_DEC:
//        {
//            iCwDec();
//        }
//        break;
        
//        case DPID_SCENCE_SAVE:
//        {
//            iSaveScence(pu8RxData[VALUE1_INDEX]);    
//        }
//        break;
        
//        case DPID_SCENCE_EXCUTE:
//        {
//            iExcuteScence(pu8RxData[VALUE1_INDEX]);    //长按保存时虽然会发送SAVE数据，但最高位会被置1，函数参数检查时就不会通过，所以此处不做另外判断
//        }
//        break;
        
        case DPID_MIX_WHETE:
        {
            if(s_sScConfig[0].eSwitch != E_SWITCH_STATUS_ON)
            {
                return;
            }
            
            s_sScConfig[0].eWorkModeType = E_WORK_MODE_MIX_WHITE;
            iRefreshAsConfig();
        }
        break;
        
        case DPID_NIGHT_LIGHT:
        {
            s_sScConfig[0].eSwitch = E_SWITCH_STATUS_NIGHT_LIGHT;
            iRefreshAsConfig();
        }
        break;
        
//        case DPID_SINGLE_WHITE:
//        {
//            if(s_sScConfig[0].eSwitch != E_SWITCH_STATUS_ON)
//            {
//                return;
//            }
//            
//            s_sScConfig[0].eWorkModeType = E_WORK_MODE_TYPE_WHITE;
//            iRefreshAsConfig();
//        }
//        break;
        
//        case DPID_TIMING:
//        {
//            iSetTimingPowerOff((((uint16)pu8RxData[VALUE2_INDEX]) << 8) | pu8RxData[VALUE1_INDEX]);
//        }
//        break;
    }
}
#endif

#ifdef SUPPORT_DEVIVE_TYPE_RGBW
else if(eDeviceType == E_DEVICE_TYPE_RGBW)
{
    switch(0x7f & pu8RxData[DATA_POINT_INDEX])
    {
        case DPID_SWITCH:
        {
            if(pu8RxData[DATA_POINT_INDEX] & 0x80)   //按键长按
            {
                return;
            }
            
            if(s_sScConfig[0].eSwitch != E_SWITCH_STATUS_OFF)
            {
                s_sScConfig[0].eSwitch = E_SWITCH_STATUS_OFF;
            }
            else if(s_sScConfig[0].eSwitch == E_SWITCH_STATUS_OFF)
            {
                s_sScConfig[0].eSwitch = E_SWITCH_STATUS_ON;
            }
            iRefreshAsConfig();
            
            for(i=0; i<20; i++)
            {
                s_u8Channel++;
                if(s_u8Channel >= RF24G_CHANNEL_NUM)
                {
                    s_u8Channel = 0;
                }
        
                RF24G_Tx(RemoterDataTemp, REMOTER_BUFF_LEN, RF24G_CH);
                while(RF24G_IsTxOrRxComplete() == E_RF24G_REC_STATUS_NO)
                {
        
                }
                Delay_ms((GulSystickCount % 3) + 1);
            } 
            RF24G_EnterRxMode(RF24G_CH);
        }
        break;
        
        case DPID_ON:
        {
            if(pu8RxData[DATA_POINT_INDEX] & 0x80)   //按键长按
            {
                s_sScConfig[0].eWorkModeType = E_WORK_MODE_ALL_WHITE;
                s_sScConfig[0].u8ColourfulDeviceBn = APS_BN_MAX;
                iRefreshAsConfig();            
            }
            else
            {
                if(s_sScConfig[0].eSwitch != E_SWITCH_STATUS_ON)
                {
                    s_sScConfig[0].eSwitch = E_SWITCH_STATUS_ON;
                    iRefreshAsConfig();
                }
            }
            
            for(i=0; i<20; i++)
            {
                s_u8Channel++;
                if(s_u8Channel >= RF24G_CHANNEL_NUM)
                {
                    s_u8Channel = 0;
                }
        
                RF24G_Tx(RemoterDataTemp, REMOTER_BUFF_LEN, RF24G_CH);
                while(RF24G_IsTxOrRxComplete() == E_RF24G_REC_STATUS_NO)
                {
        
                }
                Delay_ms((GulSystickCount % 3) + 1);
            } 
            RF24G_EnterRxMode(RF24G_CH);
        }
        break;
        
        case DPID_OFF:
        {
            if(pu8RxData[DATA_POINT_INDEX] & 0x80)   //按键长按
            {
//                s_sScConfig[0].eSwitch = E_SWITCH_STATUS_NIGHT_LIGHT;
//                iRefreshAsConfig();            
            }
            else
            {
                if(s_sScConfig[0].eSwitch != E_SWITCH_STATUS_OFF)
                {
                    s_bTimingPowerOffFlag = FALSE;  //主动关机的时候，取消定时关机  
                
                    s_sScConfig[0].eSwitch = E_SWITCH_STATUS_OFF;
                    iRefreshAsConfig();
                }
            }
            
            for(i=0; i<20; i++)
            {
                s_u8Channel++;
                if(s_u8Channel >= RF24G_CHANNEL_NUM)
                {
                    s_u8Channel = 0;
                }
        
                RF24G_Tx(RemoterDataTemp, REMOTER_BUFF_LEN, RF24G_CH);
                while(RF24G_IsTxOrRxComplete() == E_RF24G_REC_STATUS_NO)
                {
        
                }
                Delay_ms((GulSystickCount % 3) + 1);
            } 
            RF24G_EnterRxMode(RF24G_CH);
        }
        break;
        
        case DPID_SPEED_INC:
        {
            iSpeedInc();
        }
        break;      

        case DPID_SPEED_DEC:
        {
            iSpeedDec();
        }
        break;       

        case DPID_MODE_INC:
        {
            if(GulSystickCount - s_u32LastModeTime < 800)
            {
                return;
            }
            s_u32LastModeTime = GulSystickCount;
            
            s_u32LastSyncTime = GulSystickCount - 10000;
            ModeKeyFlag = TRUE;
            iModeInc();
        }
        break; 
        
        case DPID_MODE_DEC:
        {
            if(s_sScConfig[0].eWorkModeType == E_WORK_MODE_TYPE_COLOUR)
            {
                s_sScConfig[0].eWorkModeType = E_WORK_MODE_TYPE_WHITE;
                iRefreshAsConfig();
                return;
            }
            iModeDec();
        }
        break; 

        case DPID_BRIGHTNESS_INC:
        {
//            if((pu8RxData[DATA_POINT_INDEX] & 0x80) == 0)
//            {
                iBnInc();
//            }
//            else    //长按，无级调速
//            {
//                eSteplessBrightnessAdjustmentStatus = E_STEPLESS_BRIGHTNESS_ADJUSTMENT_STATUS_INC;
//                s_u32SteplessBrightnessAdjustmentStartTime = GulSystickCount;
//            }
        }
        break;      

        case DPID_BRIGHTNESS_DEC:
        {
//            if((pu8RxData[DATA_POINT_INDEX] & 0x80) == 0)
//            {
                iBnDec();
//            }
//            else    //长按，无级调速
//            {
//                eSteplessBrightnessAdjustmentStatus = E_STEPLESS_BRIGHTNESS_ADJUSTMENT_STATUS_DEC;
//                s_u32SteplessBrightnessAdjustmentStartTime = GulSystickCount;            
//            }
        }
        break; 

        case DPID_BRIGHTNESS_SET:
        {   
            iBnSet(pu8RxData[VALUE1_INDEX]);
        }
        break;          

//        case DPID_COLOUR_INC:
//        {
//            iColorInc();
//        }
//        break;
        
//        case DPID_COLOUR_DEC:
//        {
//            iColorDec();
//        }
//        break;
        
        case DPID_COLOUR_SET:
        {
            iColorSet(pu8RxData[VALUE1_INDEX]);
        }
        break;     

//        case DPID_CW_SET:
//        {
//            iCwSet(pu8RxData[VALUE1_INDEX]);
//        }
//        break;
        
//        case DPID_CW_INC:
//        {
//            iCwInc();
//        }
//        break;
        
//        case DPID_CW_DEC:
//        {
//            iCwDec();
//        }
//        break;
        
//        case DPID_SCENCE_SAVE:
//        {
//            iSaveScence(pu8RxData[VALUE1_INDEX]);    
//        }
//        break;
        
//        case DPID_SCENCE_EXCUTE:
//        {
//            iExcuteScence(pu8RxData[VALUE1_INDEX]);    //长按保存时虽然会发送SAVE数据，但最高位会被置1，函数参数检查时就不会通过，所以此处不做另外判断
//        }
//        break;
        
//        case DPID_MIX_WHETE:
//        {
//            if(s_sScConfig[0].eSwitch != E_SWITCH_STATUS_ON)
//            {
//                return;
//            }
//            
//            s_sScConfig[0].eWorkModeType = E_WORK_MODE_MIX_WHITE;
//            iRefreshAsConfig();
//        }
//        break;
        
        case DPID_NIGHT_LIGHT:
        {
            s_sScConfig[0].eSwitch = E_SWITCH_STATUS_NIGHT_LIGHT;
            iRefreshAsConfig();
        }
        break;
        
        case DPID_SINGLE_WHITE:
        {
            if(s_sScConfig[0].eSwitch != E_SWITCH_STATUS_ON)
            {
                return;
            }
            
            if(pu8RxData[DATA_POINT_INDEX] & 0x80)    //长按按键
            {
                s_sScConfig[0].eWorkModeType = E_WORK_MODE_MIX_WHITE;
                iRefreshAsConfig();            
            }
            else
            {
                s_sScConfig[0].eWorkModeType = E_WORK_MODE_TYPE_WHITE;
                iRefreshAsConfig();
            }
        }
        break;
        
//        case DPID_TIMING:
//        {
//            iSetTimingPowerOff((((uint16)pu8RxData[VALUE2_INDEX]) << 8) | pu8RxData[VALUE1_INDEX]);
//        }
//        break;
    }
}
#endif
}

void GLHOCP_EventCallBack(GLH_OCP_EVENT_E eEvent)
{
    switch(eEvent)
    {
        case E_GLH_OCP_EVENT_PROTEC:
        {
            s_bApsIsOverCurrent = TRUE;
        }
        break;
        
        case E_GLH_OCP_EVENT_UNPROTEC:
        {
            s_bApsIsOverCurrent = FALSE;
            GLHLM_Init();
            iRefreshAsConfig();            
        }
        break;
    }
}

/*
* Copyright (c) 2021, 深圳市光丽海科技有限公司
* All rights reserved.
*
* 文件名称：jt_led_mode.c
* 文件标识：
* 摘 要：
*    灯条模式的具体实现
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期：2021年7月5日
*/
#include "glh_led_mode.h"
#include "glh_rgb.h"
#include "glh_sys_tick.h"

#define     SPEED_LEVEL_NUM             10
#define     STATIC_DYNAMIC_GRAD         1
#define     DYNAMIC_GRAD                1       //动态模式的变化梯度
#define     SPEED_PARAM                 1
#define     ON_OFF_VALUE_CHANGE         1
#define     BN_VALUE_CHANGE             1
#define     ON_OFF_VALUE_MAX            255
#define     iResetCompleteFlag()        s_u8CompleteFlag = 0
#define     iSetCompleteFlag()          s_u8CompleteFlag = 1
#define     iIsComplete()               (s_u8CompleteFlag == 1)
#define     iSetRgbcw(R, G, B, C, W)    GLHRGB_SetColor(PWM_CHANNEL_R, R);GLHRGB_SetColor(PWM_CHANNEL_G, G);GLHRGB_SetColor(PWM_CHANNEL_B, B);GLHRGB_SetColor(PWM_CHANNEL_C, C);GLHRGB_SetColor(PWM_CHANNEL_W, W)
#define     MAX_BN                      255
#define     JIANBIAN_MODE_SPEED         (MAX_SPEED - s_u8Speed + 2)
#define     TIAOBIAN_MODE_SPEED         ((MAX_SPEED - s_u8Speed + 1) * 100)
#define     STATIC_MODE_ACTION_TIME     3

typedef void (* MODE_SHADE_F)(void);     //渲染线程

static MODE_SHADE_F XDATA s_pfnModeShade = NULL;       //彩光模式线程

static uint8 XDATA s_u8CurrColorNum = 0;         //总共设置了几个颜色
static uint8 XDATA s_u8ColorIndex = 0;           //当前变化到了第几个颜色
static uint8 XDATA s_u8CurrR = 0, s_u8CurrG = 0, s_u8CurrB = 0, s_u8CurrC = 0, s_u8CurrW = 0;  //当前的RGB值
static uint8 XDATA s_u8StaticTargetR = 0, s_u8StaticTargetG = 0, s_u8StaticTargetB = 0, s_u8StaticTargetC = 0, s_u8StaticTargetW = 0;  //当前的RGB值
static uint8 XDATA s_u8BretahProgressValue = 0;
static RGBCW_S XDATA s_sRgbcw[COLOR_NUM_MAX] = {0};
static uint8 XDATA s_u8Flag = 0, s_u8Flag1 = 0, s_u8Flag2 = 0;
static uint8 XDATA s_u8CompleteFlag = 0; //动作是否完成
static uint8 XDATA s_u8Speed = 100;
static uint32 XDATA u32LastRgbTime = 0;
static uint8 s_u8Cnt = 0;
static BOOL s_bSwitchStatu = OFF;
static uint8 s_u8OnOffValue = 0;
static uint16 s_u16CurrBn = 0, s_u16TargetBn = 0;
static CODE uint16 s_au16JumpModeSpeed[SPEED_LEVEL_NUM] = {1800, 1300, 1100, 900, 700, 500, 300, 250, 200, 150};
static CODE uint16 s_au16FlashModeSpeed[SPEED_LEVEL_NUM] = {1800, 1300, 1100, 900, 700, 500, 300, 250, 200, 150};
static CODE uint16 s_au16BreathModeSpeed[SPEED_LEVEL_NUM] = {13, 9, 8, 7, 6, 5, 4, 3, 2, 1};
static CODE uint16 s_au16FadeModeSpeed[SPEED_LEVEL_NUM] = {35, 30, 25, 20, 15, 10, 7, 5, 3, 2};
static uint8 s_u8Step = 0;

code uint16 u16BnTable[BN_MAX] =         //GAMMA值3.0
{
145, 
208 ,
278 ,
356 ,
440 ,
532 ,
629 ,
733 ,
842 ,
958 ,
1078 ,
1204 ,
1335 ,
1471 ,
1612 ,
1757 ,
1908 ,
2062 ,
2222 ,
2386 ,
2554 ,
2726 ,
2903 ,
3083 ,
3268 ,
3457 ,
3649 ,
3846 ,
4046 ,
4251 ,
4459 ,
4670 ,
4886 ,
5105 ,
5327 ,
5553 ,
5783 ,
6016 ,
6252 ,
6492 ,
6735 ,
6982 ,
7232 ,
7485 ,
7741 ,
8001 ,
8264 ,
8530 ,
8799 ,
9072 ,
9347 ,
9625 ,
9907 ,
10191 ,
10479 ,
10770 ,
11063 ,
11360 ,
11659 ,
11961 ,
12267 ,
12575 ,
12886 ,
13200 ,
13516 ,
13836 ,
14158 ,
14483 ,
14811 ,
15141 ,
15474 ,
15810 ,
16149 ,
16490 ,
16834 ,
17181 ,
17530 ,
17882 ,
18236 ,
18594 ,
18953 ,
19316 ,
19680 ,
20048 ,
20418 ,
20790 ,
21165 ,
21543 ,
21923 ,
22305 ,
22690 ,
23078 ,
23468 ,
23860 ,
24255 ,
24652 ,
25052 ,
25454 ,
25858 ,
26265 ,
};

void iStatic(void);
    
static void iResetParams(void)
{
	s_u8Flag = 0;
	s_u8Flag1 = 0;
	s_u8Flag2 = 0;
	s_u8Cnt = 0;
    s_u8ColorIndex = 0;
	s_u8CurrR = 0;
	s_u8CurrG = 0;
	s_u8CurrB = 0;
    s_u8CurrC = 0;
	s_u8CurrW = 0;
	s_u8BretahProgressValue = 0;
    s_u8Step = 0;
    iResetCompleteFlag();
}
    
void GLHLM_Init(void)
{
    GLHRGB_Init();
    iResetParams();
}


//处理开关机的淡入淡出
static void iOnOffFadeMainThread(void)
{
    static uint32 s_u32LastOnOffChangeTime = 0;

    if(GulSystickCount - s_u32LastOnOffChangeTime >= 1)
    {
        s_u32LastOnOffChangeTime = GulSystickCount;
        
        if(s_bSwitchStatu == ON)
        {
            if(s_u8OnOffValue >= ON_OFF_VALUE_MAX)
            {
                return;
            }
            
            if(s_u8OnOffValue == 0)
            {
                iResetParams();
                s_u16CurrBn = s_u16TargetBn;
                GLHRGB_SetBn(s_u16CurrBn);
            }
            
            if(s_u8OnOffValue + ON_OFF_VALUE_CHANGE < ON_OFF_VALUE_MAX)
            {
                s_u8OnOffValue += ON_OFF_VALUE_CHANGE;
            }
            else
            {
                s_u8OnOffValue = ON_OFF_VALUE_MAX;
            }
            GLHRGB_SetOnOffValue(s_u8OnOffValue);
        }
        else
        {
            if(s_u8OnOffValue == 0)
            {
                return;
            }
            
            if(s_u8OnOffValue > ON_OFF_VALUE_CHANGE)
            {
                s_u8OnOffValue -= ON_OFF_VALUE_CHANGE;
            }
            else
            {
                s_u8OnOffValue = 0;
                
                iResetParams();
                s_u16CurrBn = s_u16TargetBn;
                GLHRGB_SetBn(s_u16CurrBn);
            }
            GLHRGB_SetOnOffValue(s_u8OnOffValue);        
        }
    }
}

//处理亮度设置时的渐变
static void iBnFadeMainThread(void)
{
    static uint32 s_u32LastBnChangeTime = 0;
    uint16 u16BnValueChange = 0;
    uint8 i = 0;

    if(s_bSwitchStatu == OFF)
    {
        return;
    }
    
    if(GulSystickCount - s_u32LastBnChangeTime >= 5)
    {
        s_u32LastBnChangeTime = GulSystickCount;


        if(s_u16TargetBn > s_u16CurrBn)
        {
            u16BnValueChange = (s_u16TargetBn - s_u16CurrBn) >> 6;
        }
        else
        {
            u16BnValueChange = (s_u16CurrBn - s_u16TargetBn) >> 6;
        }
        if(u16BnValueChange == 0)
        {
            u16BnValueChange = 1;
        }
        
        if(s_u16CurrBn < s_u16TargetBn)
        {
            if(s_u16CurrBn + u16BnValueChange < s_u16TargetBn)
            {
                s_u16CurrBn += u16BnValueChange;
            }
            else
            {
                s_u16CurrBn = s_u16TargetBn;
            }
            GLHRGB_SetBn(s_u16CurrBn);        
        }
        else
        {
            if(s_u16TargetBn + u16BnValueChange < s_u16CurrBn)
            {
                s_u16CurrBn -= u16BnValueChange;
            }
            else
            {
                s_u16CurrBn = s_u16TargetBn;
            }
            GLHRGB_SetBn(s_u16CurrBn);
        }
    }
}

void GLHLM_RenderProc(void)
{
    if (s_pfnModeShade != NULL)
    {
        s_pfnModeShade();
    }
    
    iOnOffFadeMainThread();
    iBnFadeMainThread();
}

BOOL GLHLM_GetModeIsComplete(void)
{
    return iIsComplete();
}

static void iCheckIsComplete(void)
{
    s_u8ColorIndex++;
    if (s_u8ColorIndex >= s_u8CurrColorNum)
    {
        s_u8ColorIndex = 0;
        iSetCompleteFlag();   //一次动作执行完成
    }
    else
    {
        iResetCompleteFlag();
    }
}

void GLHLM_SetColorBuff(uint8 u8Index, uint8 u8R, uint8 u8G, uint8 u8B, uint8 u8C, uint8 u8W)
{
    if((u8Index >= COLOR_NUM_MAX))
    {
        return;
    }
    
    s_sRgbcw[u8Index].u8r = u8R;
    s_sRgbcw[u8Index].u8g = u8G;
    s_sRgbcw[u8Index].u8b = u8B;
    s_sRgbcw[u8Index].u8c = u8C;
    s_sRgbcw[u8Index].u8w = u8W;
}

void iStatic(void)
{
    static uint32 s_u32StaticLastTxTime = 0;
    
    if(s_bSwitchStatu == OFF)
    {
        return;
    }
    
    if(GulSystickCount - u32LastRgbTime < STATIC_MODE_ACTION_TIME)
    {
        return;
    }
    u32LastRgbTime = GulSystickCount;
    
    if(s_u8CurrR > s_u8StaticTargetR)
    {
        if(s_u8CurrR - s_u8StaticTargetR >= STATIC_DYNAMIC_GRAD)
        {
            s_u8CurrR -= STATIC_DYNAMIC_GRAD;
        }
        else
        {
            s_u8CurrR = s_u8StaticTargetR;
        }
    }
    else
    {
        if(s_u8StaticTargetR - s_u8CurrR >= STATIC_DYNAMIC_GRAD)
        {
            s_u8CurrR += STATIC_DYNAMIC_GRAD;
        }
        else
        {
            s_u8CurrR = s_u8StaticTargetR;
        }
    }
    
    if(s_u8CurrG > s_u8StaticTargetG)
    {
        if(s_u8CurrG - s_u8StaticTargetG >= STATIC_DYNAMIC_GRAD)
        {
            s_u8CurrG -= STATIC_DYNAMIC_GRAD;
        }
        else
        {
            s_u8CurrG = s_u8StaticTargetG;
        }
    }
    else
    {
        if(s_u8StaticTargetG - s_u8CurrG >= STATIC_DYNAMIC_GRAD)
        {
            s_u8CurrG += STATIC_DYNAMIC_GRAD;
        }
        else
        {
            s_u8CurrG = s_u8StaticTargetG;
        }
    }
    
    if(s_u8CurrB > s_u8StaticTargetB)
    {
        if(s_u8CurrB - s_u8StaticTargetB >= STATIC_DYNAMIC_GRAD)
        {
            s_u8CurrB -= STATIC_DYNAMIC_GRAD;
        }
        else
        {
            s_u8CurrB = s_u8StaticTargetB;
        }
    }
    else
    {
        if(s_u8StaticTargetB - s_u8CurrB >= STATIC_DYNAMIC_GRAD)
        {
            s_u8CurrB += STATIC_DYNAMIC_GRAD;
        }
        else
        {
            s_u8CurrB = s_u8StaticTargetB;
        }
    }
    
    if(s_u8CurrC > s_u8StaticTargetC)
    {
        if(s_u8CurrC - s_u8StaticTargetC >= STATIC_DYNAMIC_GRAD)
        {
            s_u8CurrC -= STATIC_DYNAMIC_GRAD;
        }
        else
        {
            s_u8CurrC = s_u8StaticTargetC;
        }
    }
    else
    {
        if(s_u8StaticTargetC - s_u8CurrC >= STATIC_DYNAMIC_GRAD)
        {
            s_u8CurrC += STATIC_DYNAMIC_GRAD;
        }
        else
        {
            s_u8CurrC = s_u8StaticTargetC;
        }
    }
	
    if(s_u8CurrW > s_u8StaticTargetW)
    {
        if(s_u8CurrW - s_u8StaticTargetW >= STATIC_DYNAMIC_GRAD)
        {
            s_u8CurrW -= STATIC_DYNAMIC_GRAD;
        }
        else
        {
            s_u8CurrW = s_u8StaticTargetW;
        }
    }
    else
    {
        if(s_u8StaticTargetW - s_u8CurrW >= STATIC_DYNAMIC_GRAD)
        {
            s_u8CurrW += STATIC_DYNAMIC_GRAD;
        }
        else
        {
            s_u8CurrW = s_u8StaticTargetW;
        }
    }

    iSetRgbcw(s_u8CurrR, s_u8CurrG, s_u8CurrB, s_u8CurrC, s_u8CurrW);	

    if(s_u8OnOffValue != ON_OFF_VALUE_MAX)
    {
        return;
    }
    
    if(GulSystickCount - s_u32StaticLastTxTime >= 500)    //静态模式2秒钟同步一次
    {
        s_u8Step = 0;
        s_u32StaticLastTxTime = GulSystickCount;
        ModeComepleteSyncSend();
    }
}

/*
    功能: 设置为静态常亮模式,颜色渐变
    输入: 无
    输出: 无
    说明：调用此函数前，需要调用GLHLM_SetStaticColor函数设置显示的颜色。
*/
void GLHLM_StaticMode(uint8 u8R, uint8 u8G, uint8 u8B, uint8 u8C, uint8 u8W)
{
    s_u8StaticTargetR = u8R;
    s_u8StaticTargetG = u8G;
    s_u8StaticTargetB = u8B;
    s_u8StaticTargetC = u8C;
    s_u8StaticTargetW = u8W;
    
	s_pfnModeShade = iStatic;
    u32LastRgbTime = GulSystickCount - STATIC_MODE_ACTION_TIME;  //为了使模式能够立即执行
}

/*
    功能: 设置为静态常亮模式,立即生效，不用渐变
    输入: 无
    输出: 无
    说明：调用此函数前，需要调用GLHLM_SetColorBuff函数设置显示的颜色。
*/
void GLHLM_StaticModeImmediately(uint8 u8R, uint8 u8G, uint8 u8B, uint8 u8C, uint8 u8W)
{
	iResetParams();
    
    s_u8CurrR = u8R;
    s_u8CurrG = u8G;
    s_u8CurrB = u8B;
    s_u8CurrC = u8C;
    s_u8CurrW = u8W;
    
	iSetRgbcw(s_u8CurrR, s_u8CurrG, s_u8CurrB, s_u8CurrC, s_u8CurrW);
	s_pfnModeShade = NULL;
}

void GLHLM_SetOneChannelImmediately(GLHPWM_CHANNEL_E eCh, uint8 u8Value)
{
    switch(eCh)
    {
        case PWM_CHANNEL_R:
        {
            s_u8CurrR = u8Value;
            GLHRGB_SetColor(PWM_CHANNEL_R, u8Value);
        }
        break;
        
        case PWM_CHANNEL_G:
        {
            s_u8CurrG = u8Value;
            GLHRGB_SetColor(PWM_CHANNEL_G, u8Value);
        }
        break;
        
        case PWM_CHANNEL_B:
        {
            s_u8CurrB = u8Value;
            GLHRGB_SetColor(PWM_CHANNEL_B, u8Value);
        }
        break;
        
        case PWM_CHANNEL_C:
        {
            s_u8CurrC = u8Value;
            GLHRGB_SetColor(PWM_CHANNEL_C, u8Value);
        }
        break;
        
        case PWM_CHANNEL_W:
        {
            s_u8CurrW = u8Value;
            GLHRGB_SetColor(PWM_CHANNEL_W, u8Value);
        }
        break;
    }
}

static void iFlash(void)
{
    if(s_bSwitchStatu == OFF)
    {
        return;
    }
    
    if(GulSystickCount - u32LastRgbTime < s_au16FlashModeSpeed[s_u8Speed])
    {
        return;
    }
    u32LastRgbTime = GulSystickCount;
    
    if(s_u8Flag)
    {
        s_u8Step = 2;
        
        s_u8CurrR = 0;
        s_u8CurrG = 0;
        s_u8CurrB = 0;
        s_u8CurrC = 0;
        s_u8CurrW = 0;
        iSetRgbcw(s_u8CurrR, s_u8CurrG, s_u8CurrB, s_u8CurrC, s_u8CurrW);
        ModeComepleteSyncSend();
        iCheckIsComplete();      
    }
    else
    {
        s_u8CurrR = s_sRgbcw[s_u8ColorIndex].u8r;
        s_u8CurrG = s_sRgbcw[s_u8ColorIndex].u8g;
        s_u8CurrB = s_sRgbcw[s_u8ColorIndex].u8b;
        s_u8CurrC = s_sRgbcw[s_u8ColorIndex].u8c;
        s_u8CurrW = s_sRgbcw[s_u8ColorIndex].u8w;
        iSetRgbcw(s_u8CurrR, s_u8CurrG, s_u8CurrB, s_u8CurrC, s_u8CurrW);
        if(s_u8ColorIndex == 0)    
        {
            s_u8Step = 0;       //s_u8Step为0时，接收端会直接刷新
        }
        else
        {
            s_u8Step = 1;
        }
        ModeComepleteSyncSend();
    }  
    
    s_u8Flag = !s_u8Flag;
}

/*
    功能: 设置为闪烁模式
    输入: u8ColorNum    设置闪烁的颜色个数
    输出: 无
    说明：调用此函数前，需要调用GLHLM_SetColorBuff函数设置显示的颜色。
*/
void GLHLM_FlashMode(uint8 u8ColorNum)
{
    if ((u8ColorNum == 0) || (u8ColorNum > COLOR_NUM_MAX))
    {
        return;
    }
    
    iResetParams();
    s_u8CurrColorNum = u8ColorNum;
 
    s_pfnModeShade = iFlash;
    
    u32LastRgbTime = GulSystickCount - TIAOBIAN_MODE_SPEED;  //为了使模式能够立即执行
}

static void iBth(void)
{
    if(s_bSwitchStatu == OFF)
    {
        return;
    }
    
    if(GulSystickCount - u32LastRgbTime < s_au16BreathModeSpeed[s_u8Speed])
    {
        return;
    }
    u32LastRgbTime = GulSystickCount;
    
    if(s_u8CurrColorNum == 0)
    {
        return;
    }
	
    if (s_u8Flag)
    {
		if(s_u8BretahProgressValue - DYNAMIC_GRAD > 0)
		{
            s_u8BretahProgressValue -= DYNAMIC_GRAD;
		}
		else
	    {
		    s_u8BretahProgressValue = 0;
		}

        s_u8CurrR = s_sRgbcw[s_u8ColorIndex].u8r*s_u8BretahProgressValue/255;
        s_u8CurrG = s_sRgbcw[s_u8ColorIndex].u8g*s_u8BretahProgressValue/255;
        s_u8CurrB = s_sRgbcw[s_u8ColorIndex].u8b*s_u8BretahProgressValue/255;
        s_u8CurrC = s_sRgbcw[s_u8ColorIndex].u8c*s_u8BretahProgressValue/255;
        s_u8CurrW = s_sRgbcw[s_u8ColorIndex].u8w*s_u8BretahProgressValue/255;        
		iSetRgbcw(s_u8CurrR, s_u8CurrG, s_u8CurrB, s_u8CurrC, s_u8CurrW);

        if (s_u8BretahProgressValue == 0)
        {
            s_u8Flag = 0;

            iCheckIsComplete();
        }
    }
    else
    {
		if(s_u8BretahProgressValue + DYNAMIC_GRAD <= MAX_BN)
		{
			s_u8BretahProgressValue += DYNAMIC_GRAD;	
		}
		else
	    {
			s_u8BretahProgressValue = MAX_BN;
		}

        s_u8CurrR = s_sRgbcw[s_u8ColorIndex].u8r*s_u8BretahProgressValue/255;
        s_u8CurrG = s_sRgbcw[s_u8ColorIndex].u8g*s_u8BretahProgressValue/255;
        s_u8CurrB = s_sRgbcw[s_u8ColorIndex].u8b*s_u8BretahProgressValue/255;
        s_u8CurrC = s_sRgbcw[s_u8ColorIndex].u8c*s_u8BretahProgressValue/255;
        s_u8CurrW = s_sRgbcw[s_u8ColorIndex].u8w*s_u8BretahProgressValue/255;        
		iSetRgbcw(s_u8CurrR, s_u8CurrG, s_u8CurrB, s_u8CurrC, s_u8CurrW);
        
        if(s_u8BretahProgressValue == DYNAMIC_GRAD)
        {
            if(s_u8ColorIndex == 0)    
            {
                s_u8Step = 0;               //s_u8Step为0时，接收端会直接刷新
            }
            else
            {
                s_u8Step = 1;
            }
            ModeComepleteSyncSend();
        }

        if (s_u8BretahProgressValue == MAX_BN)
        {
            s_u8Flag = 1;

        }
    }
}

/*
    功能: 设置为呼吸模式
    输入: u8ColorNum    设置呼吸的颜色个数
    输出: 无
    说明：调用此函数前，需要调用GLHLM_SetColorBuff函数设置显示的颜色。
*/
void GLHLM_BreathMode(uint8 u8ColorNum)
{
    if ((u8ColorNum == 0) || (u8ColorNum > COLOR_NUM_MAX))
    {
        return;
    }
    
    iResetParams();
    
    s_u8CurrColorNum = u8ColorNum;
    
    s_pfnModeShade = iBth;
    
    u32LastRgbTime = GulSystickCount - JIANBIAN_MODE_SPEED;  //为了使模式能够立即执行
}

static void iFade(void)
{
    if(s_bSwitchStatu == OFF)
    {
        return;
    }
    
    if(GulSystickCount - u32LastRgbTime < s_au16FadeModeSpeed[s_u8Speed])
    {
        return;
    }
    u32LastRgbTime = GulSystickCount;
    
    if(s_u8CurrColorNum == 0)
    {
        return;
    }

    if(s_u8CurrR > s_sRgbcw[s_u8ColorIndex].u8r)
    {
        if(s_u8CurrR - s_sRgbcw[s_u8ColorIndex].u8r >= DYNAMIC_GRAD)
        {
            s_u8CurrR -= DYNAMIC_GRAD;
        }
        else
        {
            s_u8CurrR = s_sRgbcw[s_u8ColorIndex].u8r;
        }
    }
    else
    {
        if(s_sRgbcw[s_u8ColorIndex].u8r - s_u8CurrR >= DYNAMIC_GRAD)
        {
            s_u8CurrR += DYNAMIC_GRAD;
        }
        else
        {
            s_u8CurrR = s_sRgbcw[s_u8ColorIndex].u8r;
        }
    }
    
    if(s_u8CurrG > s_sRgbcw[s_u8ColorIndex].u8g)
    {
        if(s_u8CurrG - s_sRgbcw[s_u8ColorIndex].u8g >= DYNAMIC_GRAD)
        {
            s_u8CurrG -= DYNAMIC_GRAD;
        }
        else
        {
            s_u8CurrG = s_sRgbcw[s_u8ColorIndex].u8g;
        }
    }
    else
    {
        if(s_sRgbcw[s_u8ColorIndex].u8g - s_u8CurrG >= DYNAMIC_GRAD)
        {
            s_u8CurrG += DYNAMIC_GRAD;
        }
        else
        {
            s_u8CurrG = s_sRgbcw[s_u8ColorIndex].u8g;
        }
    }
    
    if(s_u8CurrB > s_sRgbcw[s_u8ColorIndex].u8b)
    {
        if(s_u8CurrB - s_sRgbcw[s_u8ColorIndex].u8b >= DYNAMIC_GRAD)
        {
            s_u8CurrB -= DYNAMIC_GRAD;
        }
        else
        {
            s_u8CurrB = s_sRgbcw[s_u8ColorIndex].u8b;
        }
    }
    else
    {
        if(s_sRgbcw[s_u8ColorIndex].u8b - s_u8CurrB >= DYNAMIC_GRAD)
        {
            s_u8CurrB += DYNAMIC_GRAD;
        }
        else
        {
            s_u8CurrB = s_sRgbcw[s_u8ColorIndex].u8b;
        }
    }
    
    if(s_u8CurrC > s_sRgbcw[s_u8ColorIndex].u8c)
    {
        if(s_u8CurrC - s_sRgbcw[s_u8ColorIndex].u8c >= DYNAMIC_GRAD)
        {
            s_u8CurrC -= DYNAMIC_GRAD;
        }
        else
        {
            s_u8CurrC = s_sRgbcw[s_u8ColorIndex].u8c;
        }
    }
    else
    {
        if(s_sRgbcw[s_u8ColorIndex].u8c - s_u8CurrC >= DYNAMIC_GRAD)
        {
            s_u8CurrC += DYNAMIC_GRAD;
        }
        else
        {
            s_u8CurrC = s_sRgbcw[s_u8ColorIndex].u8c;
        }
    }
    
    if(s_u8CurrW > s_sRgbcw[s_u8ColorIndex].u8w)
    {
        if(s_u8CurrW - s_sRgbcw[s_u8ColorIndex].u8w >= DYNAMIC_GRAD)
        {
            s_u8CurrW -= DYNAMIC_GRAD;
        }
        else
        {
            s_u8CurrW = s_sRgbcw[s_u8ColorIndex].u8w;
        }
    }
    else
    {
        if(s_sRgbcw[s_u8ColorIndex].u8w - s_u8CurrW >= DYNAMIC_GRAD)
        {
            s_u8CurrW += DYNAMIC_GRAD;
        }
        else
        {
            s_u8CurrW = s_sRgbcw[s_u8ColorIndex].u8w;
        }
    }
	
    iSetRgbcw(s_u8CurrR, s_u8CurrG, s_u8CurrB, s_u8CurrC, s_u8CurrW);
	
	if((s_u8CurrR == s_sRgbcw[s_u8ColorIndex].u8r) && (s_u8CurrG == s_sRgbcw[s_u8ColorIndex].u8g)\
        &&(s_u8CurrB == s_sRgbcw[s_u8ColorIndex].u8b) && (s_u8CurrC == s_sRgbcw[s_u8ColorIndex].u8c) && (s_u8CurrW == s_sRgbcw[s_u8ColorIndex].u8w))
	{
        if(s_u8ColorIndex == 0)
        {
            s_u8Step = 0;               //s_u8Step为0时，接收端会直接刷新          
        }
        else
        {
            s_u8Step = 1;
        }
        
        ModeComepleteSyncSend(); 
		iCheckIsComplete();
	} 
}

/*
    功能: 设置为渐变模式
    输入: u8ColorNum    设置渐变的颜色个数
    输出: 无
    说明：调用此函数前，需要调用GLHLM_SetColorBuff函数设置显示的颜色。
*/
void GLHLM_FadeMode(uint8 u8ColorNum)
{
    if ((u8ColorNum == 0) || (u8ColorNum > COLOR_NUM_MAX))
    {
        return;
    }
    
    iResetParams();
	s_u8ColorIndex = 0;
    
    s_u8CurrColorNum = u8ColorNum;
    
    s_pfnModeShade = iFade;
    
    u32LastRgbTime = GulSystickCount - JIANBIAN_MODE_SPEED;  //为了使模式能够立即执行
    
    ModeComepleteSyncSend();
}

static void iJump(void)
{
    if(s_bSwitchStatu == OFF)
    {
        return;
    }
    
    if(GulSystickCount - u32LastRgbTime < s_au16JumpModeSpeed[s_u8Speed])
    {
        return;
    }
	u32LastRgbTime = GulSystickCount;

    if (s_u8ColorIndex >= s_u8CurrColorNum)
    {
        s_u8ColorIndex = 0;
        iSetCompleteFlag();   //一次动作执行完成
		u32LastRgbTime = GulSystickCount - TIAOBIAN_MODE_SPEED;
		return;
    }
    else
    {
        iResetCompleteFlag();
    }

    s_u8CurrR = s_sRgbcw[s_u8ColorIndex].u8r;
    s_u8CurrG = s_sRgbcw[s_u8ColorIndex].u8g;
    s_u8CurrB = s_sRgbcw[s_u8ColorIndex].u8b;
    s_u8CurrC = s_sRgbcw[s_u8ColorIndex].u8c;
    s_u8CurrW = s_sRgbcw[s_u8ColorIndex].u8w;    
    iSetRgbcw(s_u8CurrR, s_u8CurrG, s_u8CurrB, s_u8CurrC, s_u8CurrW);
    
    if(s_u8ColorIndex == 0)
    {
        s_u8Step = 0;
    }
    else
    {
        s_u8Step = 1;
    }
    ModeComepleteSyncSend();

	s_u8ColorIndex++;
}

/*
    功能: 设置为跳变模式
    输入: u8ColorNum    设置跳变的颜色个数
    输出: 无
    说明：调用此函数前，需要调用GLHLM_SetColorBuff函数设置显示的颜色。
*/

void GLHLM_JumpMode(uint8 u8ColorNum)
{
    if ((u8ColorNum == 0) || (u8ColorNum > COLOR_NUM_MAX))
    {
        return;
    }
    
    iResetParams();
    
    s_u8CurrColorNum = u8ColorNum;
    
    s_pfnModeShade = iJump;
    
    u32LastRgbTime = GulSystickCount - 1000000;  //为了使模式能够立即执行,一般的闪烁间隔可定不会超过1000秒，所以减去1000秒应该没问题
}

static void iFireWorks(void)
{
    if(s_bSwitchStatu == OFF)
    {
        return;
    }
    
    if(GulSystickCount - u32LastRgbTime < JIANBIAN_MODE_SPEED)
    {
        return;
    }
    u32LastRgbTime = GulSystickCount;
    
    if(s_u8CurrColorNum == 0)
    {
        return;
    }
	
    if (s_u8Flag)
    {
		if(s_u8BretahProgressValue + DYNAMIC_GRAD <= MAX_BN)
		{
			s_u8BretahProgressValue += DYNAMIC_GRAD;	
		}
		else
	    {
			s_u8BretahProgressValue = MAX_BN;
		}

        if (s_u8BretahProgressValue == MAX_BN)
        {
            s_u8Flag = 0;
            iCheckIsComplete();
        }
    }
    else
    {
		if(s_u8BretahProgressValue - DYNAMIC_GRAD > 0)
		{
            s_u8BretahProgressValue -= DYNAMIC_GRAD;
		}
		else
	    {
		    s_u8BretahProgressValue = 0;
		}

        s_u8CurrR = s_sRgbcw[s_u8ColorIndex].u8r*s_u8BretahProgressValue/255;
        s_u8CurrG = s_sRgbcw[s_u8ColorIndex].u8g*s_u8BretahProgressValue/255;
        s_u8CurrB = s_sRgbcw[s_u8ColorIndex].u8b*s_u8BretahProgressValue/255;
        s_u8CurrC = s_sRgbcw[s_u8ColorIndex].u8c*s_u8BretahProgressValue/255;
        s_u8CurrW = s_sRgbcw[s_u8ColorIndex].u8w*s_u8BretahProgressValue/255;
		iSetRgbcw(s_u8CurrR, s_u8CurrG, s_u8CurrB, s_u8CurrC, s_u8CurrW);

        if (s_u8BretahProgressValue == 0)
        {
            s_u8Flag = 1;
        }
    }
}

/*
    功能: 设置为烟花模式
    输入: u8ColorNum    设置烟花模式的颜色个数
    输出: 无
    说明：调用此函数前，需要调用GLHLM_SetColorBuff函数设置显示的颜色。
*/
void GLHLM_FireWorksMode(uint8 u8ColorNum)
{
    if ((u8ColorNum == 0) || (u8ColorNum > COLOR_NUM_MAX))
    {
        return;
    }
    
    iResetParams();
    s_u8BretahProgressValue = MAX_BN;
    
    s_u8CurrColorNum = u8ColorNum;
    
    s_pfnModeShade = iFireWorks;
    
    u32LastRgbTime = GulSystickCount - JIANBIAN_MODE_SPEED;  //为了使模式能够立即执行
}

static void iOneBreathAndThreeJump(void)
{
    if(s_bSwitchStatu == OFF)
    {
        return;
    }

    if(s_u8Flag == 0)
	{
		if(GulSystickCount - u32LastRgbTime < s_au16BreathModeSpeed[s_u8Speed])
		{
			return;
		}
		u32LastRgbTime = GulSystickCount;
    
		if(s_u8CurrColorNum == 0)
		{
			return;
		}
        
		if (s_u8Flag1)
		{
			if(s_u8BretahProgressValue - DYNAMIC_GRAD > 0)
			{
				s_u8BretahProgressValue -= DYNAMIC_GRAD;
			}
			else
			{
				s_u8BretahProgressValue = 0;
			}

            s_u8CurrR = s_sRgbcw[s_u8ColorIndex].u8r*s_u8BretahProgressValue/255;
            s_u8CurrG = s_sRgbcw[s_u8ColorIndex].u8g*s_u8BretahProgressValue/255;
            s_u8CurrB = s_sRgbcw[s_u8ColorIndex].u8b*s_u8BretahProgressValue/255;
            s_u8CurrC = s_sRgbcw[s_u8ColorIndex].u8c*s_u8BretahProgressValue/255;
            s_u8CurrW = s_sRgbcw[s_u8ColorIndex].u8w*s_u8BretahProgressValue/255;
            iSetRgbcw(s_u8CurrR, s_u8CurrG, s_u8CurrB, s_u8CurrC, s_u8CurrW);            

			if (s_u8BretahProgressValue == 0)
			{
				s_u8Flag = 1;
                s_u8Flag1 = 0;
			}
		}
		else
		{         
            if(s_u8BretahProgressValue == 0)
            {
                if(s_u8ColorIndex == 0)    
                {
                    s_u8Step = 0;               //s_u8Step为0时，接收端会直接刷新
                }
                else
                {
                    s_u8Step = 1;
                }
                ModeComepleteSyncSend();
            }
            
			if(s_u8BretahProgressValue + DYNAMIC_GRAD <= MAX_BN)
			{
				s_u8BretahProgressValue += DYNAMIC_GRAD;	
			}
			else
			{
				s_u8BretahProgressValue = MAX_BN;
			}

            s_u8CurrR = s_sRgbcw[s_u8ColorIndex].u8r*s_u8BretahProgressValue/255;
            s_u8CurrG = s_sRgbcw[s_u8ColorIndex].u8g*s_u8BretahProgressValue/255;
            s_u8CurrB = s_sRgbcw[s_u8ColorIndex].u8b*s_u8BretahProgressValue/255;
            s_u8CurrC = s_sRgbcw[s_u8ColorIndex].u8c*s_u8BretahProgressValue/255;
            s_u8CurrW = s_sRgbcw[s_u8ColorIndex].u8w*s_u8BretahProgressValue/255;
            iSetRgbcw(s_u8CurrR, s_u8CurrG, s_u8CurrB, s_u8CurrC, s_u8CurrW); 

			if (s_u8BretahProgressValue == MAX_BN)
			{
				s_u8Flag1 = 1;
			}            
		}	
	}
	else
    {
		if(GulSystickCount - u32LastRgbTime < s_au16FlashModeSpeed[s_u8Speed])
		{
			return;
		}
		u32LastRgbTime = GulSystickCount;
    
		if(s_u8Flag2)
		{
			iSetRgbcw(0, 0, 0, 0, 0);
            s_u8Step = (s_u8Cnt << 4) + 4;
            ModeComepleteSyncSend();
		}
		else
		{
			if(s_u8Cnt < 3)
			{
                s_u8CurrR = s_sRgbcw[s_u8ColorIndex].u8r;
                s_u8CurrG = s_sRgbcw[s_u8ColorIndex].u8g;
                s_u8CurrB = s_sRgbcw[s_u8ColorIndex].u8b;
                s_u8CurrC = s_sRgbcw[s_u8ColorIndex].u8c;
                s_u8CurrW = s_sRgbcw[s_u8ColorIndex].u8w;	                
                iSetRgbcw(s_u8CurrR, s_u8CurrG, s_u8CurrB, s_u8CurrC, s_u8CurrW);  

                s_u8Step = (s_u8Cnt << 4) + 3;
                ModeComepleteSyncSend();                  
			}
            else
			{
				s_u8Cnt = 0;
				s_u8Flag = 0;
				s_u8Flag1 = 0;
				s_u8Flag2 = 0;
				s_u8BretahProgressValue = 0;
                iCheckIsComplete();
                return;
			} 
            s_u8Cnt++;
		}  
        
		s_u8Flag2 = !s_u8Flag2;	
	}
}

/*
    功能: 设置为一呼吸三跳变模式
    输入: u8ColorNum    设置一呼吸三跳变颜色个数
    输出: 无
    说明：调用此函数前，需要调用GLHLM_SetColorBuff函数设置显示的颜色。
*/
void GLHLM_OneBthAndThreeJumpMode(uint8 u8ColorNum)
{
    if ((u8ColorNum == 0) || (u8ColorNum > COLOR_NUM_MAX))
    {
        return;
    }
    
    iResetParams();
    
    s_u8CurrColorNum = u8ColorNum;
    
    s_pfnModeShade = iOneBreathAndThreeJump;
    u32LastRgbTime = GulSystickCount - 100000;  //为了使模式能够立即执行
}

/*
    功能: 设置速度
    输入: u8Speed    速度参数
    输出: 无
    说明：
*/
void GLHLM_SetSpeed(uint8 u8Speed)
{
//	if(u8Speed > MAX_SPEED)
//    {
//		s_u8Speed = MAX_SPEED;
//	}
//	else if(u8Speed < MIN_SPEED)
//    {
//		s_u8Speed = MIN_SPEED;
//	}
//    else
//    {
        s_u8Speed = u8Speed - 1;
//    }
}

void GLHLM_SetBn(uint8 u8bn)
{
    if(u8bn > BN_MAX)
    {
        return;
    }
    
    s_u16TargetBn = u16BnTable[u8bn - 1];
}

void GLHLM_SetSwitchStatus(BOOL bSwitch)
{
    s_bSwitchStatu = bSwitch;
}

void GLHLM_GetCurrRgbw(uint8 *pu8R, uint8 *pu8G, uint8 *pu8B, uint8 *pu8W)
{
    *pu8R = s_u8CurrR;
    *pu8G = s_u8CurrG;
    *pu8B = s_u8CurrB;
    *pu8W = s_u8CurrW;
}

void GLHLM_SetCurrRgbw(uint8 u8R, uint8 u8G, uint8 u8B, uint8 u8W)
{
    s_u8CurrR = u8R;
    s_u8CurrG = u8G;
    s_u8CurrB = u8B;
    s_u8CurrW = u8W;
}

uint8 GLHLM_GetCurrColorIndex(void)
{
    return s_u8ColorIndex;
}

void GLHLM_SetCurrColorIndex(uint8 u8ColorIndex)
{
    s_u8ColorIndex = u8ColorIndex;
}

uint8 GLHLM_GetStep(void)
{
    return s_u8Step;
}

void GLHLM_SetStep(uint8 u8Step)
{
    s_u8Step = u8Step;
    
    if(s_pfnModeShade == iFlash)
    {
        switch(s_u8Step)
        {
            case 0:
            {
                s_u8Flag = 0;
            }
            break;
            
            case 1:
            {
                s_u8Flag = 0;
            }
            break;
            
            case 2:
            {
                s_u8Flag = 1;
            }
            break;
            
            default:
                break;
        }
    }
    else if(s_pfnModeShade == iBth)
    {
        if((u8Step == 0) || (u8Step == 1))
        {
            s_u8BretahProgressValue = 0;
            s_u8Flag = 0;
        }
//        else if(u8Step == 2)
//        {
//            s_u8BretahProgressValue = MAX_BN;
//            s_u8Flag = 1;
//        }
    }    
    else if(s_pfnModeShade == iFade)
    {

    }
    else if(s_pfnModeShade == iOneBreathAndThreeJump)
    {
        if((u8Step == 0) || (u8Step == 1))
        {
            s_u8Flag = 0;
            s_u8Flag1 = 0;
            s_u8BretahProgressValue = 0;
        }
        else if((u8Step & 0x0f) == 3)
        {
            s_u8Flag = 1;
            s_u8Flag2 = 0;
            s_u8Cnt = (s_u8Step >> 4) & 0x0f;
        }
        else if((u8Step & 0x0f) == 4)
        {
            s_u8Flag = 1;
            s_u8Flag2 = 1;
            s_u8Cnt = (s_u8Step >> 4) & 0x0f;
        }
    }
    else if(s_pfnModeShade == iJump)
    {
        
    }
    
    u32LastRgbTime = GulSystickCount - 100000;
}

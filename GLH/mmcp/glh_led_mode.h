/*
* Copyright (c) 2022, 深圳市光丽海科技有限公司
* All rights reserved.
*
* 文件名称：glh_led_mode.h
* 文件标识：
* 摘 要：
*    灯条模式的具体实现
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期：2022年6月18日
*/
#ifndef __GLH_LED_MODE_H__
#define __GLH_LED_MODE_H__
#include "glh_typedef.h"
#include "glh_rgb.h"
#include "glh_pwm.h"

#ifndef COLOR_NUM_MAX
#define COLOR_NUM_MAX            8
#endif

#ifndef MAX_SPEED
#define MAX_SPEED                100
#endif

#ifndef MIN_SPEED
#define MIN_SPEED                1
#endif

typedef struct _RGBCW_S
{
    uint8 u8r;
    uint8 u8g;
    uint8 u8b;
    uint8 u8c;
    uint8 u8w;
}RGBCW_S;
extern void ModeComepleteSyncSend(void);
void GLHLM_Init(void);
void GLHLM_SetColorBuff(uint8 u8Index, uint8 u8R, uint8 u8G, uint8 u8B, uint8 u8C, uint8 u8W);
void GLHLM_RenderProc(void);
void GLHLM_StaticModeImmediately(uint8 u8R, uint8 u8G, uint8 u8B, uint8 u8C, uint8 u8W);
void GLHLM_StaticMode(uint8 u8R, uint8 u8G, uint8 u8B, uint8 u8C, uint8 u8W);
void GLHLM_FlashMode(uint8 u8ColorNum);
void GLHLM_BreathMode(uint8 u8ColorNum);
void GLHLM_FadeMode(uint8 u8ColorNum);
void GLHLM_JumpMode(uint8 u8ColorNum);
void GLHLM_FireWorksMode(uint8 u8ColorNum);
void GLHLM_OneBthAndThreeJumpMode(uint8 u8ColorNum);
void GLHLM_SetSpeed(uint8 u8Speed);
BOOL GLHLM_GetModeIsComplete(void);
void GLHLM_SetBn(uint8 u8bn);
void GLHLM_SetSwitchStatus(BOOL bColourfulDeviceSwitch);
void GLHLM_SetOneChannelImmediately(GLHPWM_CHANNEL_E eCh, uint8 u8Value);
void GLHLM_GetCurrRgbw(uint8 *pu8R, uint8 *pu8G, uint8 *pu8B, uint8 *pu8W);
void GLHLM_SetCurrRgbw(uint8 u8R, uint8 u8G, uint8 u8B, uint8 u8W);
uint8 GLHLM_GetCurrColorIndex(void);
void GLHLM_SetCurrColorIndex(uint8 u8ColorIndex);
uint8 GLHLM_GetStep(void);
void GLHLM_SetStep(uint8 u8Step);

#endif //__GLH_LED_MODE_H__




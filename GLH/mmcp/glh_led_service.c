/*
* Copyright (c) 2021, 深圳市光丽海科技有限公司
* All rights reserved.
*
* 文件名称：jt_lec_service.c
* 文件标识：
* 摘 要：
*   主要逻辑层的实现
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期：2021年6月18日
*/

#include "glh_sys_tick.h"
#include "glh_led_mode.h"
#include "glh_led_service.h"

/*
    功能: SCS层初始化
    输入: 无
    输出: 无
*/
void GLHSCS_Init(void)
{
    GLHLM_Init();
}


/*
    功能: SCS层线程
    输入: 无
    输出: 无
*/
void GLHSCS_MainThread(void)
{
	GLHLM_RenderProc();
}

/*
    功能: 设置RGB显示buffer
    输入: 无
    输出: 无
*/
void GLHSCS_SetRgbBuff(uint8 u8Index, RGBW_S sRgb)
{
	GLHLM_SetColorBuff(u8Index, sRgb);
}


/*
    功能: RGB设置为静态模式
    输入: 无
    输出: 无
    说明：调用此函数前，需要调用GLHSCS_SetRgbBuff函数设置显示的颜色。
*/
void GLHSCS_SetRgbModeStatic(void)
{
	GLHLM_StaticMode();
}


/*
    功能: RGB设置为闪烁模式
    输入: u8ColorNum    设置闪烁的颜色个数
    输出: 无
    说明：调用此函数前，需要调用GLHSCS_SetRgbBuff函数设置显示的颜色。
*/
void GLHSCS_SetRgbModeFlash(uint8 u8ColorNum)
{
	GLHLM_FlashMode(u8ColorNum);
}


/*
    功能: RGB设置为渐变模式
    输入: u8ColorNum    设置渐变的颜色个数
    输出: 无
    说明：调用此函数前，需要调用GLHSCS_SetRgbBuff函数设置显示的颜色。
*/
void GLHSCS_SetRgbModeFade(uint8 u8ColorNum)
{
	GLHLM_FadeMode(u8ColorNum);
}


/*
    功能: RGB设置为跳变模式
    输入: u8ColorNum    设置跳变的颜色个数
    输出: 无
    说明：调用此函数前，需要调用GLHSCS_SetRgbBuff函数设置显示的颜色。
*/
void GLHSCS_SetRgbModeJump(uint8 u8ColorNum)
{
	GLHLM_JumpMode(u8ColorNum);
}

/*
    功能: RGB设置为关机模式
    输入: 无
    输出: 无
    说明：
*/
void GLHSCS_SetRgbModeOff(void)
{
	GLHLM_RgbPowerOffMode();
}

/*
    功能: RGB设置为开机模式
    输入: 无
    输出: 无
    说明：
*/
void GLHSCS_SetRgbModeOn(void)
{
	GLHLM_RgbPowerOnMode();
}


/*
    功能: 白光设置为关机模式
    输入: 无
    输出: 无
    说明：
*/
void GLHSCS_SetWhiteModeOff(void)
{
	GLHLM_WhitePowerOffMode();
}

/*
    功能: 白光设置为开机模式
    输入: 无
    输出: 无
    说明：
*/
void GLHSCS_SetWhiteModeOn(void)
{
	GLHLM_WhitePowerOnMode();
}

/*
    功能: 设置白光亮度
    输入: u8WhiteBrightNess 白光的亮度
    输出: 无
    说明：
*/
void GLHSCS_SetWhiteBrightNess(uint8 u8WhiteBrightNess)
{
	GLHLM_SetWhiteBrightness(u8WhiteBrightNess);
}

/*
    功能: 设置白光冷暖值
    输入: u8WhiteTemprature 白光的冷暖值
    输出: 无
    说明：
*/
void GLHSCS_SetWhiteTemprature(uint8 u8WhiteTemprature)
{
	GLHLM_SetWhiteTemprature(u8WhiteTemprature);
}

/*
    功能: 设置速度
    输入: u8Speed 速度值,值越大，速度越快
    输出: 无
    说明：
*/
void GLHSCS_SetSpeed(uint8 u8Speed)
{
	GLHLM_SetSpeed(u8Speed);
}
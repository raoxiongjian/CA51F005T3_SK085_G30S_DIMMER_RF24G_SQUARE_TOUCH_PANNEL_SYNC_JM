/*
* Copyright (c) 2022, 深圳市光丽海科技有限公司
* All rights reserved.
*
* 文件名称：glh_rgb.c
* 文件标识：
* 摘 要：
*   rgb颜色驱动实现模块
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期: 2021年7月5日
*/
#include "glh_typedef.h"
#include "glh_rgb.h"

static uint16 s_u16Bn = 0;
static uint8 s_u8OnOffStateValue = 0;            //用来处理开关机淡入淡出效果
static uint8 s_u8LastR = 0, s_u8LastG = 0, s_u8LastB = 0, s_u8LastC = 0, s_u8LastW = 0;

void GLHRGB_Init(void)
{
    GLHPWM_Init(PWM_CHANNEL_R | PWM_CHANNEL_G | PWM_CHANNEL_B | PWM_CHANNEL_C | PWM_CHANNEL_W);
}


void GLHRGB_SetColor(GLHPWM_CHANNEL_E eCh, uint8 u8Value)
{
    GLHPWM_Set(eCh, (uint32)u8Value*s_u16Bn*s_u8OnOffStateValue);
    switch(eCh)
    {
        case PWM_CHANNEL_R:
        {
            s_u8LastR = u8Value;
        }
        break;
        
        case PWM_CHANNEL_G:
        {
            s_u8LastG = u8Value;
        }
        break;
        
        case PWM_CHANNEL_B:
        {
            s_u8LastB = u8Value;
        }
        break;
        
        case PWM_CHANNEL_C:
        {
            s_u8LastC = u8Value;
        }
        break;
        
        case PWM_CHANNEL_W:
        {
            s_u8LastW = u8Value;
        }
        break;
    }
}

void GLHRGB_SetBn(uint16 u16Bn)
{
    s_u16Bn = u16Bn;
	
    GLHPWM_Set(PWM_CHANNEL_R, (uint32)s_u8LastR*s_u16Bn*s_u8OnOffStateValue);
    GLHPWM_Set(PWM_CHANNEL_G, (uint32)s_u8LastG*s_u16Bn*s_u8OnOffStateValue);
    GLHPWM_Set(PWM_CHANNEL_B, (uint32)s_u8LastB*s_u16Bn*s_u8OnOffStateValue);
    GLHPWM_Set(PWM_CHANNEL_C, (uint32)s_u8LastC*s_u16Bn*s_u8OnOffStateValue);
    GLHPWM_Set(PWM_CHANNEL_W, (uint32)s_u8LastW*s_u16Bn*s_u8OnOffStateValue);
}

void GLHRGB_SetOnOffValue(uint8 u8OnOffStateValue)
{
    s_u8OnOffStateValue = u8OnOffStateValue;
	
    GLHPWM_Set(PWM_CHANNEL_R, (uint32)s_u8LastR*s_u16Bn*s_u8OnOffStateValue);
    GLHPWM_Set(PWM_CHANNEL_G, (uint32)s_u8LastG*s_u16Bn*s_u8OnOffStateValue);
    GLHPWM_Set(PWM_CHANNEL_B, (uint32)s_u8LastB*s_u16Bn*s_u8OnOffStateValue);
    GLHPWM_Set(PWM_CHANNEL_C, (uint32)s_u8LastC*s_u16Bn*s_u8OnOffStateValue);
    GLHPWM_Set(PWM_CHANNEL_W, (uint32)s_u8LastW*s_u16Bn*s_u8OnOffStateValue);
}

/*
* Copyright (c) 2021, 深圳市光丽海科技有限公司
* All rights reserved.
*
* 文件名称：jt_adc.c
* 文件标识：
* 摘 要：
*    adc模块
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期: 2021年7月16日
*/

#include "include/adc.h"
#include "include/ca51f003_config.h"		
#include "include/ca51f003sfr.h"
#include "include/ca51f003xsfr.h"
#include "include/gpiodef_f003.h"
#include "glh_adc.h"

void GLHADC_Init(uint16 u16AdcCH)
{
    if (u16AdcCH & JTADC_CHANNEL_8)
    {
		P30F = P30_ADC8_SETTING;	//设置P11为ADC引脚功能
		ADCON = AST(0) | ADIE(1) | HTME(3) | VSEL(ADC_REF_VDD);				//设置ADC参考电压为VDD
		ADOPC = GAIN(NO_AMP);
		ADCFGL = ACKD(7) | ADCALE(1) | ADCHS(ADC_CH8);	//选择ADC0通道
    }
}

uint16 GLHADC_Get(uint16 u16AdcCH)
{
    uint16 u16AdcVal = 0;

    switch (u16AdcCH)
    {
		case JTADC_CHANNEL_8:
			ADCON |= AST(1);																//启动ADC转换
			while(!(ADCON & ADIF));													        //等待ADC转换完成
			ADCON |= ADIF;																	//清除ADC中断标志
			u16AdcVal = ADCDH*256 + ADCDL;										            //读取AD值
			u16AdcVal >>= 4;		
			break;
		default:
            break;
    }
	
    return u16AdcVal;
}
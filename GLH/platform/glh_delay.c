/*
* Copyright (c) 2021, 深圳光丽海科技有限公司
* All rights reserved.
*
* 文件名称：glh_delay.c
* 文件标识：
* 摘 要：
*   不同平台的延时
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期：2021年6月25日
*/
#include "glh_delay.h"
#include "glh_watchdog.h"

void GLHDL_DelayMs(uint16 ms)
{
    uint16 i = 0;
     
    while (ms--)
    {
        for (i=0; i<2001; i++)
        {
            __nop();
        }

        GLHWD_Clear();
    }
}

void GLHDL_DelayUs(uint16 us)
{
    __disable_irq();
    while (us--)
    {
        __nop();
        __nop();
        __nop();
        __nop();
        __nop();
        __nop();
        __nop();
        __nop();
        __nop();
    }
    __enable_irq();
}




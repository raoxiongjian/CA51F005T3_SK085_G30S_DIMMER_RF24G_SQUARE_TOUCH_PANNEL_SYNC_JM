/*
* Copyright (c) 2021, 深圳市光丽海科技有限公司
* All rights reserved.
*
* 文件名称：glh_flash.c
* 文件标识：
* 摘 要：
*    读写flash模块
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期：2021年6月21日
*/
#include "glh_flash.h"
#include "flash.h"
#include "include/ca51f003xsfr.h"

#ifdef SUPPORT_FLASH

void GLHFLASH_Init(void)
{
	PADRD = 0x80 - 4;
}

void GLHFLASH_EraseBlock(uint32 u32Addr, uint16 u16Len)
{
    uint8 i = 0;
	
	for(i=u32Addr/GLHFLASH_PAGE_SIZE; i<=(u32Addr+u16Len)/GLHFLASH_PAGE_SIZE; i++)
	{
		Data_Area_Sector_Erase(i);
	}
}

//uint8 GLHFLASH_ReadByte(uint32 u32Addr)
//{

//}

//void GLHLASH_WriteByte(uint32 u32Addr, uint8 u8Byte)
//{
//    
//}

void GLHFLASH_ReadBytes(uint32 u32Addr, uint8 *pu8Buff, uint16 u16Len)
{
    Data_Area_Mass_Read(u32Addr, pu8Buff, u16Len);
}


void GLHFLASH_WriteBytes(uint32 u32Addr, uint8 *pu8Buff, uint16 u16Len)
{
	GLHFLASH_EraseBlock(u32Addr, u16Len);
    Data_Area_Mass_Write(u32Addr, pu8Buff, u16Len);
}

void GLHFLASH_ReadDevId(uint8 *pu8Buff)
{
	unsigned long int ID;
    
	FSCMD = 0x80;	
	PTSH = 0x00;				
	PTSL = 0x00;        			
	FSCMD = 0x81;						
	ID = FSDAT;
	ID *= 256;
	ID |= FSDAT;
	ID *= 256;
	ID |= FSDAT;
	ID *= 256;
	ID |= FSDAT;
	FSCMD = 0;
    
    pu8Buff[0] = ID >> 24;
    pu8Buff[1] = ID >> 16;
    pu8Buff[2] = ID >> 8;
    pu8Buff[3] = ID;
}
#endif

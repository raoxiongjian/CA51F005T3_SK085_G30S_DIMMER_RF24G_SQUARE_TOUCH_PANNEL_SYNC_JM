/*
* Copyright (c) 2020, 深圳市君同科技有限公司
* All rights reserved.
*
* 文件名称：jt_gpio.h
* 文件标识：
* 摘 要：
*   gpio操作通用接口，只支持设置GPIO为普通GPIO状态，包括输入、输出、中断，不支持复用（PWM、AD、UART等）
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期: 2020年10月9日
*/
#ifndef __JT_GPIO_H__
#define __JT_GPIO_H__
#include "jt_typedef.h"
#include "bsp_gpio.h"

#define JTGPIO_MAX_GPIO_NUM             8       // 最大可用的gpio个数

typedef enum _JTGPIO_PIN_E
{
     	E_GPIO_PIN_0 = 0,
	E_GPIO_PIN_1, 
	E_GPIO_PIN_2, 
	E_GPIO_PIN_3,
	E_GPIO_PIN_4, 
	E_GPIO_PIN_5, 
	E_GPIO_PIN_6, 
	E_GPIO_PIN_7, 
	E_GPIO_PIN_8, 
	E_GPIO_PIN_9, 
	E_GPIO_PIN_10,
	E_GPIO_PIN_11,
	E_GPIO_PIN_12,
	E_GPIO_PIN_13,
	E_GPIO_PIN_14,
	E_GPIO_PIN_15,
	E_GPIO_PIN_16,
	E_GPIO_PIN_17,
	E_GPIO_PIN_18,
	E_GPIO_PIN_19,
	E_GPIO_PIN_20,
	E_GPIO_PIN_21,
	E_GPIO_PIN_22,
	E_GPIO_PIN_23,
	E_GPIO_PIN_24,
	E_GPIO_PIN_25,
	E_GPIO_PIN_26,
	E_GPIO_PIN_27,
	E_GPIO_PIN_28,
	E_GPIO_PIN_29,
	E_GPIO_PIN_30,
	E_GPIO_PIN_31,
}JTGPIO_PIN_E;

typedef enum _JTGPIO_OUTPUT_TYPE_E
{
    E_GPIO_OUTPUT_PP = 0, //推挽输出
    E_GPIO_OUTPUT_OD, // 开漏输出
}JTGPIO_OUTPUT_TYPE_E;

typedef enum _JTGPIO_INPUT_TYPE_E
{
    E_GPIO_INPUT_UP = GPIO_Mode_Input_Up, //上拉输入
    E_GPIO_INPUT_DOWN = GPIO_Mode_Input_Down, // 下拉输入
    E_GPIO_INPUT_FLOAT = GPIO_Mode_Input_Float, // 浮空输入
}JTGPIO_INPUT_TYPE_E;

typedef enum _JTGPIO_IRQ_TYPE_E
{
    E_GPIO_IRQ_RIS = RIS_EDGE_INT, // 上升沿中断
    E_GPIO_IRQ_FALL = FAIL_EDGE_INT, // 下降沿中断
    E_GPIO_IRQ_RIS_FALL = RIS_FAIL_EDGE_INT, // 上升-下降沿中断
}JTGPIO_IRQ_TYPE_E;

typedef void (* JTGPIO_IrqCallback_F)(void);

void JTGPIO_DirOutput(uint8 u8GpioNum, uint8 u8OutputType);

void JTGPIO_DirInput(uint8 u8GpioNum, uint8 u8InputType);

void JTGPIO_SetValue(uint8 u8GpioNum, uint8 u8Value);

uint8 JTGPIO_GetValue(uint8 u8GpioNum);

void JTGPIO_EnableIrq(uint8 u8GpioNum, uint8 u8IrqType);

void JTGPIO_DisableIrq(uint8 u8GpioNum);

void JTGPIO_RegisterIrqCallback(uint8 u8GpioNum, JTGPIO_IrqCallback_F pfnCallback);

//引脚复用功能
//void JTGPIO_ConfigAf(uint8 u8GpioNum, uint8 u8Af);

//void JTGPIO_ConfigMux(uint8 u8GpioNum, uint8 u8Mux);

#endif //__JT_GPIO_H__
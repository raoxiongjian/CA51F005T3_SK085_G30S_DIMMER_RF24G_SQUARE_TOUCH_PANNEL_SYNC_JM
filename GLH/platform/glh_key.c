/*
* Copyright (c) 2020, 深圳市君同科技有限公司
* All rights reserved.
*
* 文件名称：jt_key.c
* 文件标识：
* 摘 要：
*   按键处理，支持唤醒
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期：2020年6月1日
*/
#include "jt_key.h"
#include "jt_sys_tick.h"

#ifdef SUPPORT_KEY

#define iReadKeyGpio() gpio_input_val(JTKEY_GPIO)

#if (JTKEY_VALID == 0)
#define iKeyGpioIsValid() (iReadKeyGpio() == 0)
#else
#define iKeyGpioIsValid() (iReadKeyGpio() != 0)
#endif

#define JTKEY_CODE          0
//#define JTKEY_INVALID   1       //无效电平
#define JTKEY_THREAD_INTERVAL   20     // 10ms检测一次
#define JTKEY_LONG_KEY_TIME   1000
#define JTKEY_DOUBLE_INTERVAL       200     // 200ms内双击两次都算双击

static uint32 s_u32KeyTick = 0;
static uint8 s_u8KeyCnt = 0;
static BOOL s_bNeedKeyup = FALSE;
static uint8 s_u8DoubleKeyDownTick = 0; //双击按键检测

void JTKEY_Init(void)
{
    gpio_mux_ctl(JTKEY_GPIO,0);
    gpio_fun_sel(JTKEY_GPIO, GPIO_Dx);
    gpio_fun_inter(JTKEY_GPIO,0);
#if (JTKEY_VALID == 0)
    gpio_direction_input(JTKEY_GPIO, GPIO_Mode_Input_Up);//swd  
#else
    gpio_direction_input(JTKEY_GPIO, GPIO_Mode_Input_Down);//swd  
#endif

    s_u8KeyCnt = 0;
    s_u8DoubleKeyDownTick = 0;
    s_bNeedKeyup = FALSE;
    s_u32KeyTick = systick_get_next_tick(JTKEY_THREAD_INTERVAL);
}

void JTKEY_DeInit(void)
{
    //
}

//按键检测
inline static void iKeyCheck(void)
{
    if (iKeyGpioIsValid())    //按键有效
    {
        if (s_u8KeyCnt < (JTKEY_LONG_KEY_TIME/JTKEY_THREAD_INTERVAL))
        {
            s_u8KeyCnt++;
            if (s_u8KeyCnt == (JTKEY_LONG_KEY_TIME/JTKEY_THREAD_INTERVAL))
            {
                JTKEY_EventCb(JTKEY_CODE, E_KEY_ACTION_LONG_KEYDOWN);
            }
        }
    }
    else
    {
        if (s_u8KeyCnt == 0)
            goto CHECK_DOUBLE_TICK;
    
        if ((s_u8KeyCnt < (JTKEY_DOUBLE_INTERVAL/JTKEY_THREAD_INTERVAL)) && (s_u8DoubleKeyDownTick == 0))
        {
            s_u8DoubleKeyDownTick = (JTKEY_DOUBLE_INTERVAL/JTKEY_THREAD_INTERVAL);
        }
        else if (s_u8KeyCnt >= (JTKEY_LONG_KEY_TIME/JTKEY_THREAD_INTERVAL))
        {
            s_u8DoubleKeyDownTick = 0;
            JTKEY_EventCb(JTKEY_CODE, E_KEY_ACTION_LONG_KEYUP);
        }
        else 
        {
            if (s_u8DoubleKeyDownTick == 0)     //单击
            {
                JTKEY_EventCb(JTKEY_CODE, E_KEY_ACTION_SINGLE_CLINCK);
            }
            else    //双击
            {
                s_u8DoubleKeyDownTick = 0;
                JTKEY_EventCb(JTKEY_CODE, E_KEY_ACTION_DOUBLE_KEY);
            }
        }

CHECK_DOUBLE_TICK:
        if (s_u8DoubleKeyDownTick > 0)
        {
            s_u8DoubleKeyDownTick--;
            if (s_u8DoubleKeyDownTick == 0)
            {
                // 单击
                JTKEY_EventCb(JTKEY_CODE, E_KEY_ACTION_SINGLE_CLINCK);
            }
        }

        s_u8KeyCnt = 0;
    }
}

void JTKEY_MainThread(void)
{
    if (systick_after_eq(s_u32KeyTick))
    {
//        if ((s_bNeedKeyup == FALSE) || (gpio_read(JTKEY_GPIO) != JTKEY_VALID))
//        {
//            s_bNeedKeyup = FALSE;
//            iKeyCheck();
//        }
        if (!s_bNeedKeyup)
        {
            iKeyCheck();
        }
        else
        {
            if (!iKeyGpioIsValid()) //无效的时候，代表key已经抬起了
            {
                s_bNeedKeyup = FALSE;
            }
        }

        s_u32KeyTick = systick_get_next_tick(JTKEY_THREAD_INTERVAL);
    }
}

//设置需要按键抬起
void JTKEY_SetNeedKeyup(BOOL bNeed)
{
    s_bNeedKeyup = bNeed;
}

BOOL JTKEY_GetKeyIsValid(void)
{
    return iKeyGpioIsValid();
}

#endif

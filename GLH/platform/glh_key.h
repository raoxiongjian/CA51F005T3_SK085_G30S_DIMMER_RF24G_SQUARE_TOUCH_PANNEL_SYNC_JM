/*
* Copyright (c) 2020, 深圳市君同科技有限公司
* All rights reserved.
*
* 文件名称：jt_key.h
* 文件标识：
* 摘 要： 
*   按键处理，支持唤醒，采用查询方式实现
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期：2020年6月1日
*/
#ifndef __JT_KEY_H__
#define __JT_KEY_H__
#include "jt_typedef.h"

#ifdef SUPPORT_KEY

// 按键GPIO定义
#ifndef JTKEY_GPIO
#error "Must define JTKEY_GPIO"
#endif

// 按键有效电平定义
#ifndef JTKEY_VALID
#define JTKEY_VALID   0
#endif
#endif

//按键动作
typedef enum _JTKEY_ACTION_E
{
    E_KEY_ACTION_UNKOWN = 0, 
    E_KEY_ACTION_SINGLE_CLINCK,     //单击
    E_KEY_ACTION_LONG_KEYDOWN,   //长按键按下
    E_KEY_ACTION_LONG_KEYUP,         //长按键抬起
    E_KEY_ACTION_DOUBLE_KEY, //双击
}JTKEY_ACTION_E;

/**
    @功能: 按键事件回调
    @参数: 
            u8Code[in]: 按键键值
            u8Action[in]: 按键动作，参考JTKEY_ACTION_E
    @返回: 
*/
extern void JTKEY_EventCb(uint8 u8Code, uint8 u8Action);

/**
    @功能: 初始化按键模块
    @参数:
    @返回:
*/
void JTKEY_Init(void);


/**
    @功能: 恢复按键模块到默认状态
    @参数:
    @返回:
*/
void JTKEY_DeInit(void);

/**
    @功能: 按键主线程
    @参数:
    @返回:
*/
void JTKEY_MainThread(void);

/**
    @功能: 设置是否需要等待按键抬起
    @参数: 
        bNeed[in]: TRUE代表需要，反之不需要等待按键抬起
    @返回:
*/
void JTKEY_SetNeedKeyup(BOOL bNeed);

/**
    @功能: 获取当前按键是否有效
    @参数: 
    @返回: TRUE代表有效
*/
BOOL JTKEY_GetKeyIsValid(void);

#endif //__JT_KEY_H__

/*
* Copyright (c) 2022, 深圳市光丽海科技有限公司
* All rights reserved.
*
* 文件名称：glh_pwm.h
* 文件标识：
* 摘 要：
*     通过定时器产生pwm
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期：2022年8月3日
*/
#include "include/ca51f003_config.h"		
#include "include/ca51f003sfr.h"
#include "include/ca51f003xsfr.h"
#include "include/gpiodef_f003.h"
#include "include/system_clock.h"
#include "glh_over_current_protect.h"
#include "glh_sys_tick.h"

#ifdef SUPPORT_OVER_CURRENT_PROTECT

#define OVER_CURRENT_PROTECT_TIME     2000
#define PROTECT_TRIG_IO_LEVEL         1
#define AMPAEN(N)				(N<<7)	  //n=0~1
#define AMPA_INP_PD(N)	(N<<6)	  //n=0~1
#define AMPBEN(N)				(N<<5)	  //n=0~1
#define AMPB_SM_EN(N)			(N<<4)	  //n=0~1
#define AMPB_T_EN(N)			(N<<3)	  //n=0~1
#define CODE_DETA(N)			(N<<2)	  //n=0~1
#define AMPA_SM_EN(N)			(N<<1)	  //n=0~1

static BOOL s_bIsOverCurrent = FALSE;
static uint32 s_u32OverCurrentStartTime = 0;

enum 
{
	P00_INDEX = 0,
	P01_INDEX = 1,
	P02_INDEX = 2,
	P03_INDEX = 3,
	P04_INDEX = 4,
	P05_INDEX = 5,
	P06_INDEX = 6,
	P07_INDEX = 7,
	P10_INDEX = 8,
	P11_INDEX = 9,
	P12_INDEX = 10,
	P13_INDEX = 11,
	P14_INDEX = 12,
	P15_INDEX = 13,
	P16_INDEX = 14,
	P17_INDEX = 15,
	P20_INDEX = 16,
	P30_INDEX = 17,

};

void GLHOCP_Init(void)
{
    //使能运放A
    AMPCON = (AMPCON&0x39) | AMPAEN(1) | AMPA_INP_PD(0) | AMPA_SM_EN(1);   //使能运放A  AMP_A的正端接入到PAD 施密特输出
    
	P05F = P05_OPAINP_SETTING;
	P06F = P06_OPAINN_SETTING;
	P07F = P07_OPAOUT_SETTING;
    
    //外部中断配置
	P30F = INPUT;						    //P30设置为输入功能
	IT0CON = P30_INDEX;			            //设置P30为INT0中断引脚
	EX0 = 1;								//INT0中断使能
	IE0 = 1;								//外部中断0中断使能
	IT0 = 0;								//设置INT0为上升沿触发
	PX0 = 1;								//设置INT0中断为最高优先级
    
    PWMBD = 0x00;                           //PWM刹车后每一路的电平都为低电平
}

void GLHOCP_MainThread(void)
{
    if(s_bIsOverCurrent == TRUE)       //处于保护状态
    {
        if(GulSystickCount - s_u32OverCurrentStartTime > OVER_CURRENT_PROTECT_TIME)    //超过解除保护的时间
        {
            s_bIsOverCurrent = FALSE;
//            GLHOCP_EventCallBack(E_GLH_OCP_EVENT_UNPROTEC);                             //解除保护
            PWMSBC = 0x00;                                                              //所有的PWM通道解除刹车
        }
    }
}

void INT0_ISR (void) interrupt 0
{
    PWMSBC = 0xff;                                  //所有的PWM通道刹车
//    GLHOCP_EventCallBack(E_GLH_OCP_EVENT_PROTEC);   //执行保护操作
    s_bIsOverCurrent = TRUE;
    s_u32OverCurrentStartTime = GulSystickCount;    //记录保护开始的时间
}

#endif
/*
* Copyright (c) 2016, 深圳市光丽海科技有限公司
* All rights reserved.
*
* 文件名称：jt_pwm.h
* 文件标识：
* 摘 要：
*     通过定时器产生pwm
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期：2021年6月16日
*/
#include "glh_pwm.h"
#include "include/ca51f003_config.h"		
#include "include/ca51f003sfr.h"
#include "include/ca51f003xsfr.h"
#include "include/gpiodef_f003.h"
#include "include/system_clock.h"

#define PWMDIV_V				(FOSC/3000)		                        //当PWM时钟为其他时钟频率时，需相应修改参数
#define PWMDUT_V				(PWMDIV_V)			                        //占空比为50%
#define PWM_FREQUENCY_DIV       0

#ifdef SUPPORT_PWM
extern void Uart0_PutChar(unsigned char bdat);
extern unsigned char temp;
void GLHPWM_Init(uint8 u8Ch)
{
	CKCON |= IHCKE;					//打开IRCH时钟
	
    if(u8Ch & E_PWM_CH0)
    {
		INDEX = PWM_CH0;												      //设置INDEX值对应PWM0
		PWMCON = TIE(0) | ZIE(0) | PIE(0) | NIE(0) | MS(0) | CKS_IH ;		  //设置PWM时钟源为IRCH  

		PWMCFG = TOG(0) | PWM_FREQUENCY_DIV;	    //时钟3分频			
		PWMPS = PWM_P11_INDEX;				                                  //设置P01为PWM0输出引脚
		P11F  = PWM_SETTING;	
	
		//设置PWMDIV、PWMDUT
		PWMDIVH	= (unsigned char)(PWMDIV_V>>8);			
		PWMDIVL	= (unsigned char)(PWMDIV_V);			
		PWMDUTH	= 0;		
		PWMDUTL	= 0;	

		PWMUPD = (1<<PWM_CH0);									             //PWMDIV、PWMDUT更新使能
		while(PWMUPD);													     //等待更新完成
		PWMEN |= (1<<PWM_CH0);										         //PWM0使能
	}
	
    if(u8Ch & E_PWM_CH1)
    {
		INDEX = PWM_CH0;												     //设置INDEX值对应PWM0
		PWMCON = TIE(0) | ZIE(0) | PIE(0) | NIE(0) | MS(0) | CKS_IH ;		 //设置PWM时钟源为IRCH  

		INDEX = PWM_CH1;
		PWMCFG = TOG(0) | PWM_FREQUENCY_DIV;	    //时钟3分频				
		PWMPS = PWM_P10_INDEX;				                                 //设置P05为PWM1输出引脚
		P10F  = PWM_SETTING;	
	
		//设置PWMDIV、PWMDUT
		PWMDIVH	= (unsigned char)(PWMDIV_V>>8);			
		PWMDIVL	= (unsigned char)(PWMDIV_V);			
		PWMDUTH	= 0;		
		PWMDUTL	= 0;	

		PWMUPD = (1<<PWM_CH1);									              //PWMDIV、PWMDUT更新使能
		while(PWMUPD);													      //等待更新完成
		PWMEN |= (1<<PWM_CH1);										          //PWM0使能
	}

    if(u8Ch & E_PWM_CH2)
    {
		INDEX = PWM_CH2;												      //设置INDEX值对应PWM2
		PWMCON = TIE(0) | ZIE(0) | PIE(0) | NIE(0) | MS(0) | CKS_IH ;		  //设置PWM时钟源为IRCH  

		PWMCFG = TOG(0) | PWM_FREQUENCY_DIV;	    //时钟3分频				
		PWMPS = PWM_P00_INDEX;				                                  //设置P03为PWM0输出引脚
		P00F  = PWM_SETTING;	
	
		//设置PWMDIV、PWMDUT
		PWMDIVH	= (unsigned char)(PWMDIV_V>>8);			
		PWMDIVL	= (unsigned char)(PWMDIV_V);			
		PWMDUTH	= 0;		
		PWMDUTL	= 0;	

		PWMUPD = (1<<PWM_CH2);									             //PWMDIV、PWMDUT更新使能
		while(PWMUPD);													     //等待更新完成
		PWMEN |= (1<<PWM_CH2);										         //PWM2使能
	}
	
    if(u8Ch & E_PWM_CH3)
    {
		INDEX = PWM_CH2;												     //设置INDEX值对应PWM0
		PWMCON = TIE(0) | ZIE(0) | PIE(0) | NIE(0) | MS(0) | CKS_IH ;		 //设置PWM时钟源为IRCH  

		INDEX = PWM_CH3;		
		PWMCFG = TOG(0) | PWM_FREQUENCY_DIV;	    //时钟3分频				
		PWMPS = PWM_P01_INDEX;				                                 //设置P00为PWM0输出引脚
		P01F  = PWM_SETTING;	
	
		//设置PWMDIV、PWMDUT
		PWMDIVH	= (unsigned char)(PWMDIV_V>>8);			
		PWMDIVL	= (unsigned char)(PWMDIV_V);			
		PWMDUTH	= 0;		
		PWMDUTL	= 0;	

		PWMUPD = (1<<PWM_CH3);									             //PWMDIV、PWMDUT更新使能
		while(PWMUPD);											             //等待更新完成
		PWMEN |= (1<<PWM_CH3);										         //PWM0使能
	}
}

void GLHPWM_Set(GLHPWM_CHANNEL_E eCh, uint32 u32Duty)
{
	if(eCh & E_PWM_CH0)
    {
       INDEX = PWM_CH0;
	}
	else if(eCh & E_PWM_CH1)
    {
		INDEX = PWM_CH1;
	}
	else if(eCh & E_PWM_CH2)
    {
		INDEX = PWM_CH2;
	}
	else if(eCh & E_PWM_CH3)
    {
       INDEX = PWM_CH3;
	}
	else if(eCh & E_PWM_CH4)
    {
		INDEX = PWM_CH4;
	}
	else if(eCh & E_PWM_CH5)
    {
		INDEX = PWM_CH5;
	}
	
	PWMDUTH	=	(unsigned char)(((float)u32Duty * 1.0 / GLHPWM_MAX * PWMDUT_V) / 256);		
	PWMDUTL	= (unsigned char)(((float)u32Duty * 1.0 / GLHPWM_MAX * PWMDUT_V));	

	PWMUPD = (1<<INDEX);									//PWMDIV、PWMDUT更新使能
	if(eCh & E_PWM_CH0)
    {
       PWMUPD = (1<<PWM_CH0);
	}
	else if(eCh & E_PWM_CH1)
    {
		PWMUPD = (1<<PWM_CH1);
	}
	else if(eCh & E_PWM_CH2)
    {
		PWMUPD = (1<<PWM_CH2);
	}
	else if(eCh & E_PWM_CH3)
    {
       PWMUPD = (1<<PWM_CH3);
	}
	else if(eCh & E_PWM_CH4)
    {
		PWMUPD = (1<<PWM_CH4);
	}
	else if(eCh & E_PWM_CH5)
    {
		PWMUPD = (1<<PWM_CH5);
	}
//	while(PWMUPD);													//等待更新完成			
}
#endif
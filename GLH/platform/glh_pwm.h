/*
* Copyright (c) 2021, 深圳市光丽海科技有限公司
* All rights reserved.
*
* 文件名称：glh_pwm.h
* 文件标识：
* 摘 要： 
*     通过定时器产生pwm
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期: 2021年7月5日
*/
#ifndef __GLH_PWM_H__
#define __GLH_PWM_H__
#include "glh_typedef.h"

#ifdef SUPPORT_PWM

#ifndef GLHPWM_MAX
#define GLHPWM_MAX             0x65cc3499//pwm最大值    255*255*255
#endif

typedef enum _GLHPWM_CHANNEL_E
{
    E_PWM_CH0 = 0x01,    //PWM0   
    E_PWM_CH1 = 0x02,    //PWM1   
    E_PWM_CH2 = 0x04,    //PWM2   
    E_PWM_CH3 = 0x08,    //PWM3  
    E_PWM_CH4 = 0x10,    //PWM4  
    E_PWM_CH5 = 0x20,    //PWM5 
}GLHPWM_CHANNEL_E;

//PWMCON
#define TIE(N)			(N<<7)
#define ZIE(N)			(N<<6)
#define PIE(N)			(N<<5)
#define NIE(N)			(N<<4)
#define MS(N)			(N<<3)		//中心对齐模式
#define MOD(N)			(N<<0)	    //互补模式

#define CKS_SYS			(0<<0)
#define CKS_IH			(1<<0)
#define CKS_IL			(2<<0)
#define CKS_XH			(3<<0)

//PWMPS

#define PWM_P00_INDEX   0 
#define PWM_P01_INDEX   1 
#define PWM_P02_INDEX   2 
#define PWM_P03_INDEX   3 
#define PWM_P04_INDEX   4 
#define PWM_P05_INDEX   5 
#define PWM_P06_INDEX   6 
#define PWM_P07_INDEX   7 
#define PWM_P10_INDEX   8
#define PWM_P11_INDEX   9
#define PWM_P12_INDEX   10
#define PWM_P13_INDEX   11
#define PWM_P14_INDEX   12
#define PWM_P15_INDEX   13
#define PWM_P16_INDEX   14
#define PWM_P17_INDEX   15
#define PWM_P20_INDEX   16
#define PWM_P30_INDEX   17


//PWMCFG
#define TOG(N)			(N<<7)


//PWMFBC
#define PWMFBIF				(1<<7)
#define PWMFBIE(N)			(N<<5)
#define PWMFBL(N)			(N<<1)
#define PWMFBE(N)			(N<<0)


//PWMFBS
#define PWMFBS5(N)			(N<<5)
#define PWMFBS4(N)			(N<<4)
#define PWMFBS3(N)			(N<<3)
#define PWMFBS2(N)			(N<<2)
#define PWMFBS1(N)			(N<<1)
#define PWMFBS0(N)			(N<<0)


//PWMBD
#define PWMFBD5(N)			(N<<5)
#define PWMFBD4(N)			(N<<4)
#define PWMFBD3(N)			(N<<3)
#define PWMFBD2(N)			(N<<2)
#define PWMFBD1(N)			(N<<1)
#define PWMFBD0(N)			(N<<0)



//PWMAIF   PWMBIF	PWMCIF 	PWMDIF
#define  TIF1	(1<<7)
#define  ZIF1	(1<<6)
#define  PIF1	(1<<5)
#define  NIF1	(1<<4)
#define  TIF0	(1<<3)
#define  ZIF0	(1<<2)
#define  PIF0	(1<<1)
#define  NIF0	(1<<0)	

#define PWM_CH0		0
#define PWM_CH1		1
#define PWM_CH2		2
#define PWM_CH3		3
#define PWM_CH4		4
#define PWM_CH5		5
#define PWM_CH6		6
#define PWM_CH7		7

/*
    功能: 初始化IO控制模块
    输入: un8Ch   需要初始化的通道集合
*/
void GLHPWM_Init(uint8 u8Ch);



/*
    功能: 设置每个输出通道的PWM占空比,用于PWM控制方式
    输入:
        duty       占空比
*/
void GLHPWM_Set(GLHPWM_CHANNEL_E eCh, uint32 u32Duty);

#endif //__GLH_PWM_H__

#endif //ifdef SUPPORT_PWM


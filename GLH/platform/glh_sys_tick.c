/*
* Copyright (c) 2021, 深圳市光丽海科技有限公司
* All rights reserved.
*
* 文件名称：jt_sys_tick.c
* 文件标识：
* 摘 要：
*   通过定时器，实现开机到当前的时间
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期：2021年7月5日
*/
#include "glh_typedef.h"
#include "glh_sys_tick.h"
#include "ca51f003_config.h"
#include "CA51F003SFR.h"
#include "ca51f003xsfr.h"
#include "gpiodef_f003.h"

#define INT_TIME			1000			//定时时间，单位为us
#define	TH_VAL				(unsigned char)((0x2000 - (INT_TIME*(FOSC/1000))/12000)>>5)
#define	TL_VAL				(unsigned char)(0x2000 - (INT_TIME*(FOSC/1000))/12000)&0x1F
	
volatile uint32 GulSystickCount = 0;  //系统计数器，开机到现在所经历的时间，ms

static void iInitCLK(void)
{
    
}

static void iTimerInit(void)
{
	EA = 1;													//开全局中断

	TMOD = (TMOD&0xFC)|0x00; 	        //模式选择: 定时器0，模式0。
	TH0 = TH_VAL;    					//高8位装初值
	TL0 = TL_VAL;    					//低8位装初值
	
	TR0 = 1;                            //定时器0使能  
	ET0 = 1;                            //定时器0中断使能
	PT0 = 0;                            //设置定时器0中断优先级为低优先级
}

void GLHST_Init(void)
{ 
    iTimerInit();
}

void GLHST_Wakeup(void)
{

}

void GLHST_Pause(void)
{

}

void GLHST_Start(void)
{

}

void TIMER0_ISR (void) interrupt 1 		 //每1ms产生中断
{
	TH0 = TH_VAL;
	TL0 = TL_VAL;
	
	GulSystickCount++;
}

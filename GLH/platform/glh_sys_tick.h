/*
* Copyright (c) 2020, 深圳市光丽海科技有限公司
* All rights reserved.
*
* 文件名称：glh_sys_tick.h
* 文件标识：
* 摘 要：
*   系统时钟模块，通过定时器，实现开机到当前的时间
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期: 2021年6月17日
*/
#ifndef __GLH_SYS_TICK_H__
#define __GLH_SYS_TICK_H__
#include "glh_typedef.h"

#ifndef GLHST_TICK_TIME
#define GLHST_TICK_TIME         10   // tick每增加一次的时间间隔, ms
#endif

#define g_uxSysTick   GulSystickCount  //系统计数器，开机到现在所经历的tick时间

extern volatile uint32 GulSystickCount;

/**
    @功能: 系统时钟模块初始化
    @输入:
    @返回:
*/
void GLHST_Init(void);

/**
    @功能: 系统时钟模块反初始化，一般在睡眠前调用
    @输入:
    @返回:
*/
void GLHST_ReInit(void);

/**
    @功能: 暂停系统时钟
    @输入:
    @返回:
*/
void GLHST_Pause(void);

/**
    @功能: 重新启动时钟
    @输入:
    @返回:
*/
void GLHST_Start(void);

/**
    @功能: 时钟轮询函数 
    @参数:
    @返回:
*/
void GLHST_MainThread(void);

#endif //__JT_SYS_TICK_H__

/*
* Copyright (c) 2021, 深圳市光丽海科技有限公司
* All rights reserved.
*
* 文件名称：glh_uart.h
* 文件标识：
* 摘 要：
*   串口通信程序，模拟实现
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期：2021年6月18日
*/
#ifndef __GLH_UART_H__
#define __GLH_UART_H__
#include "glh_typedef.h"

/*
    功能: 串口接收数据单字节回调函数，应用层自己实现
	输入: u16UartRecNum：  一帧数据中的第几个 ，从0开始计数
    输入: u8byte：          第u16UartRecNum个收到的数据
*/

extern void GLHUART_RecByteCallback(const uint8 u8Byte);

/*
    功能: 串口初始化
*/
void GLHUART_Init(void);

/*
    功能: 低功耗的时候调用
*/
void GLHUART_ReInit(void);

/*
    功能: 通过串口发送一个字节
    输入: u8Byte 需要发送的字节
*/
void GLHUART_SendByte(uint8 u8Byte);

/*
    功能: 通过串口发送一组数据
    输入: pu8Buff   需要发送的数据缓冲区  
                  u8Len     数据长度
*/
void GLHUART_SendBuff(const uint8 *pu8Buff, uint8 u8Len);

/*
    功能: 串口模块主循环，必须要在main中重复调用
*/
void GLHUART_MainLoop(void);


#endif //__GLH_UART_H__



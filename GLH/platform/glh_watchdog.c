/*
* Copyright (c) 2020, 深圳市光丽海科技有限公司
* All rights reserved.
*
* 文件名称：glh_watchdog.c
* 文件标识：
* 摘 要：
*   看门狗
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期: 2021年7月26日
*/
#include "glh_watchdog.h"
#include "include/wdt.h"
#include "include/ca51f003sfr.h"


void GLHWD_Init(void)
{
/***************************看门狗阈值设置计算公式如下****************************
看门狗触发时间 = (WDVTH * 800H+7FFH) * clock cycle
*********************************************************************************/

//看门狗时钟源为IRCH(内部高速时钟)	
	WDCON = WDTS_IRCH | WDRE_reset;   	//设置看门狗时钟源为IRCH，模式为复位模式
	WDVTHH = 0x1e;																//看门狗中断阈值高八位设置 当前值为1s
	WDVTHL = 0x84;																//看门狗中断阈值低八位设置

//看门狗时钟源为IRCL
// 	CKCON |= ILCKE;															 //使能IRCL
// 	WDCON = WDTS_IRCL | WDRE_reset;   //设置看门狗时钟源为ILCKE，模式为复位模式
// 	WDVTHH = 0;																	 //看门狗中断阈值高八位设置 当前值为1s	
// 	WDVTHL = 0x0f;															 //看门狗中断阈值低八位设置

//看门狗时钟源为XOSCH(外部高速时钟)	
// 	GPIO_Init(P17F,P17_XOSCH_OUT_SETTING);	//设置外部高速时钟引脚功能
// 	GPIO_Init(P30F,P30_XOSCH_IN_SETTING);
// 	CKCON &= ~XHCS;
// 	CKCON |= XHCKE;													//使能外部高速时钟
// 	
// 	WDCON = WDTS_XOSCH | WDRE_reset;   						//设置看门狗时钟源为XOSCH，模式为复位模式
// 	WDVTHH = 0x1e;																//看门狗中断阈值高八位设置 当前值为1s
// 	WDVTHL = 0x84;																//看门狗中断阈值低八位设置	  
}

void GLHWD_Start(void)
{

}

void GLHWD_Stop(void)
{

}

void GLHWD_Clear(void)
{
	WDFLG = 0xA5;			//喂狗	
}


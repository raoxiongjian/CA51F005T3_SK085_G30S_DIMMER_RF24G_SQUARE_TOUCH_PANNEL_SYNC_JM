#ifndef WDT_H
#define WDT_H

//WDFLG����
#define WDIF		(1<<1)
#define WDRF		(1<<0)

#define WDTS_DISABLE	(0<<5)
#define WDTS_IRCH			(1<<5)
#define WDTS_IRCL			(2<<5)
#define WDTS_XOSCH		(3<<5)

#define WDRE_reset			(1<<0)
#define WDRE_int				(0<<0)

#endif